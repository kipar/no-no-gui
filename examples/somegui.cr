require "../src/nonogui"

class MainForm < GUI::Form
  @counter = 0
  @progress = 50
  @dialog = Form2.new(text: "Some GUI test", width: 640, height: 480)

  def gui
    on_mouse_move do |evt|
      text = "X=#{evt.x}, Y=#{evt.y}"
      self.text = text
    end
    # width 1024
    # height 768
    slider(align: al(:top), height: 50) do
      value @progress
      on_change do |evt|
        @progress = evt.value
      end
    end
    progressbar(align: al(:top), height: 50) do
      value @progress
    end

    panel(x: 100, y: 10, width: 125, height: 150, text: "Ошибка") do
      text "Панель #{@counter}"
      button(x: 10, y: 10, width: 100, height: 25, text: "+1") do
        on_click do
          @counter += 1

          if @dialog.visible
            @dialog.hide
          else
            @dialog.show
          end
        end
      end
      button(x: 10, y: 30, width: 100, height: 25, text: "-1") do
        on_click do
          @counter -= 1
          self.fullscreen = false
        end
      end
    end
    flow do
      scrollable true
      height 400
      align :bottom
      @progress.times do |i|
        panel(width: 100, height: 400) do
          slave = checkbox(align: al(:top), text: "Слейв")
          checkbox(align: al(:top), text: "Мастер") do |master|
            on_click do
              slave.checked = master.checked
            end
          end
          l = label(x: 10, y: 30, text: "Фрейм #{i}")
          edit(align: al(:top), x: 10, y: 10, width: 50) do |e|
            on_change do |evt|
              l.text = e.text
              if n = e.text.to_i?
                @counter = n
              end
            end
          end
        end
      end
    end
    flow(height: 200) do
      align :bottom
      autowrap true
      scrollable true
      @counter.times do |i|
        button(width: 100, height: 25, margin: 5, x: 5, text: "#{i}") do |b|
          on_click do
            b.text = (b.text.to_i + 1).to_s
          end
        end
      end
    end
  end
end

class Form2 < GUI::Form
  def gui
    # GLPanel
    flow(align: al(:top), height: 75, childmargin: 10) do
      button(text: "Button", width: 100, height: 50) do
        on_click do
          p "click"
        end
      end
      edit(text: "Edit", width: 100, height: 50) do |e|
        on_change do
          p e.text
        end
      end
      label(text: "Label", width: 100, height: 50)
      checkbox(text: "CheckBox", width: 100, height: 50) do |c|
        on_change do
          p c.checked
        end
      end
    end
    flow(align: al(:top), height: 150) do
      memo(text: "Memo\nSecond line", width: 100, height: 100) do |m|
        on_change do
          p m.text
        end
      end
      listbox(text: "ListBox\nSecondItem", width: 100, height: 100) do |l|
        on_change do
          p l.index
        end
      end
      combobox(text: "ComboBox", width: 100, height: 100) do |l|
        on_change do
          p l.index, l.text
        end
      end
      slider(width: 100, vertical: true) do |s|
        on_change do
          p s.value
        end
      end
      progressbar(width: 100, height: 100, vertical: true, value: 50)
    end
    pages(align: al(:client)) do |pg|
      on_change do
        p pg.index
      end
      page(text: "Page1") do |p2|
        on_select do
          p2.text = p2.text + "!"
        end
      end
      page(text: "Page2") do |p2|
        edit(text: p2.text, width: 100, height: 50) do |e|
          on_change do
            p2.text = e.text
          end
        end
      end
      page(text: "Page3") do
        flow do
          align :client
          autowrap true
          ellipse(width: 50, height: 50, pen: 0xFFu32, brush: 0xFF00u32)
          rect(width: 50, height: 50, pen: 0xFFu32, brush: 0xFF00u32)
          triaup(width: 50, height: 50, pen: 0xFFu32, brush: 0xFF00u32)
          triadown(width: 50, height: 50, pen: 0xFFu32, brush: 0xFF00u32)
          trialeft(width: 50, height: 50, pen: 0xFFu32, brush: 0xFF00u32)
          triaright(width: 50, height: 50, pen: 0xFFu32, brush: 0xFF00u32)
          diamond(width: 50, height: 50, pen: 0xFFu32, brush: 0xFF00u32)
        end
      end
    end
  end
end

p "GO!"
GUI.run(MainForm.new(text: "Some GUI test", fullscreen: true))
