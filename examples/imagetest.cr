require "../src/nonogui"

class MainForm < GUI::Form
  getter! ibutton : GUI::ImageButton

  def gui
    flow(align: al(:top), height: 200) do
      picture image: RES::Arrow::Left do
        on_click do
          p "picture1"
        end
      end
      picture image: RES::Arrow::Right do
        on_click do
          p "picture2"
        end
      end
      checkbox "enabled", checked: true do |cb|
        on_change do
          ibutton.enabled = cb.checked
        end
      end
      @ibutton = imagebutton(
        width: 200,
        image_normal: RES::Edit::Button::Normal,
        image_hover: RES::Edit::Button::Hovered,
        image_pressed: RES::Edit::Button::Pressed,
        image_disabled: RES::Edit::Button::Disabled
      ) do
        on_click do
          p "picture3"
        end
        on_mouse_border do |evt|
          p "user on_enter" if evt.entered
        end
      end
    end
  end
end

GUI.load_images "./examples/images"
GUI.run(MainForm.new(text: "Some GUI test", width: 640, height: 480))
