require "../src/nonogui"

class Fragment < GUI::Fragment
  def gui(data)
    panel(width: 100, height: 250) do
      label(align: al(:top)) do
        text data.count.to_s
      end
      edit(align: al(:top)) do |e|
        text data.count.to_s
        on_change do |evt|
          p evt
          data.count = e.text.to_i? || 0
        end
      end
      button(align: al(:top), height: 50, text: "+1") do |e|
        on_click do
          data.count += 1
        end
      end
      button(align: al(:top), height: 50, text: "-1") do |e|
        on_click do
          data.count -= 1
        end
      end
    end
  end
end

class Data
  property count = 0

  def initialize(@count)
  end
end

class MainForm < GUI::Form
  @data = [] of Data
  @frag = Fragment.new

  def initialize(**args)
    super(**args)
    10.times { |i| @data << Data.new(i) }
  end

  def gui
    panel(align: al(:top), height: 50) do
      flow(align: al(:client)) do
        button(width: 50, text: "-1") do
          @data.pop
        end
        button(width: 50, text: "+1") do
          @data << Data.new(0)
        end
      end
    end
    flow(align: al(:client), scrollable: true) do
      @data.each { |x| @frag.gui(x) }
    end
  end
end

p "GO!"
GUI.run(MainForm.new(text: "Some GUI test", width: 640, height: 480))
