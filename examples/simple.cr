require "../src/nonogui"

class MainForm < GUI::Form
  def gui
    flow do
      align :client
      label "Hello world", x: 20, y: 20
      button "Click me!" do
        on_click do
          puts "Button clicked"
        end
      end
    end
  end
end

GUI.run(MainForm.new(width: 800, height: 600))
