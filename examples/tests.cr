require "../src/libnonogui"

w = NONOGUI.create_window
NONOGUI.set_visible w, true

btn = NONOGUI.create_button(w)
NONOGUI.set_align(btn, NONOGUI::Align::Top)
NONOGUI.set_text(btn, "Привет, мир!")
# ui_component_set_onclick(btn, @Click)
NONOGUI.set_parent(btn, w)
counter = 0
shouldquit = false

loop do
  counter += 1
  NONOGUI.set_text(btn, "Привет, мир! #{counter}")
  unless NONOGUI.process
    shouldquit = true
  end
  break if shouldquit
end
