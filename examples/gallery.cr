require "../src/nonogui"

class BasicControls < GUI::Fragment
  def gui(owner, **args)
    flow **args, vertical: true, childmargin: 10 do
      label "Label UTF8(Привет)\ncan be multiline" do
        on_click do
          owner.event "Label on_click"
        end
      end
      button "Button" do
        on_click do
          owner.event "Button on_click"
        end
      end
      edit "Edit" do |e|
        on_change do
          owner.event "Edit on_change: " + e.text
        end
        on_click do
          owner.event "Edit on_click"
        end
      end
      memo text: "Memo\nwith multiline\npiece of text" do |m|
        on_change do
          owner.event "Memo on_change"
        end
        on_click do
          owner.event "Memo on_click"
        end
      end
      checkbox "CheckBox" do |c|
        on_change do
          owner.event "CheckBox on_change: #{c.checked}"
        end
        on_click do
          owner.event "CheckBox on_click"
        end
      end
      listbox text: "ListBox\nwith several\nitems" do |l|
        on_change do
          owner.event "ListBox on_change #{l.index}"
        end
        on_click do
          owner.event "ListBox on_click"
        end
      end
      combobox text: "ComboBox", items: ["Item1", "Item2", "Item3"].join('\n') do |l|
        on_change do
          owner.event "ComboBox on_change #{l.index}, #{l.text}"
        end
        on_click do
          owner.event "ComboBox on_click"
        end
      end
      label "Slider and ProgressBar:"
      s = slider value: 25, y: -10 do |sl|
        on_change do
          owner.event "Slider on_change #{sl.value}"
        end
        on_click do
          owner.event "Slider on_click"
        end
      end
      progressbar value: 25 do
        value s.value
        on_click do
          owner.event "ProgressBar on_click"
        end
      end
      flow height: 140, childmargin: 10 do
        s = slider vertical: true, value: 25 do |sl|
          on_change do
            owner.event "Slider on_change #{sl.value}"
          end
          on_click do
            owner.event "Slider on_click"
          end
        end
        progressbar value: 25, vertical: true, height: 100, width: 20 do
          value s.value
          on_click do
            owner.event "ProgressBar on_click"
          end
        end
      end
    end
  end
end

class FontsDemo < GUI::Fragment
  def gui(owner, **args)
    flow **args, vertical: true, childmargin: 10 do
      label "Default font"
      (4..34).step(4) do |i|
        label "font_size: #{i}", font_size: i
      end
      label "font_color: RED", font_color: 255u32
      label "font_name: Fixedsys", font_name: "Fixedsys"
      NONOGUI::FontStyle.each do |s|
        label "font_style: #{s}", font_style: s
      end
      label "font_style: All", font_style: NONOGUI::FontStyle::All
    end
  end
end

class BasicEvents < GUI::Fragment
  def gui(owner, **args)
    flow **args, vertical: true, childmargin: 10 do
      label "on_mouse_move:"
      label "on_mouse_button:", y: -10
      rect width: 200 do
        on_mouse_move do |evt|
          owner.event "on_mouse_move x=#{evt.x} y=#{evt.y}"
        end
        on_mouse_button do |evt|
          owner.event "on_mouse_button down=#{evt.down}, x=#{evt.x} y=#{evt.y}"
        end
      end
      label "on_mouse_border:"
      rect width: 200 do
        on_mouse_border do |evt|
          if evt.entered
            owner.event "on_mouse_border entered"
          else
            owner.event "on_mouse_border left"
          end
        end
      end
      label "on_keyboard:"
      edit "press something", width: 200 do
        on_keyboard do |evt|
          owner.event "on_keyboard: scancode=#{evt.scancode} down=#{evt.down}"
        end
      end
    end
  end
end

class Shapes < GUI::Fragment
  def gui(owner, **args)
    flow **args, childmargin: 10, autowrap: true do
      label "Shapes:"
      ellipse(width: 50, height: 50)
      rect(width: 50, height: 50)
      triaup(width: 50, height: 50)
      triadown(width: 50, height: 50)
      trialeft(width: 50, height: 50)
      triaright(width: 50, height: 50)
      diamond(width: 50, height: 50)
      wrap
      ellipse(width: 50, height: 50, pen: 0x0000FFu32)
      ellipse(width: 50, height: 50, pen: 0x00FF00u32)
      ellipse(width: 50, height: 50, pen: 0xFF0000u32)
      wrap
      ellipse(width: 50, height: 50, brush: 0x0000FFu32)
      ellipse(width: 50, height: 50, brush: 0x00FF00u32)
      ellipse(width: 50, height: 50, brush: 0xFF0000u32)
    end
  end
end

class GroupingControls < GUI::Fragment
  @horiz_scroll = true
  @vert_scroll = true

  def gui(owner, **args)
    flow **args, childmargin: 10, vertical: true do
      pages do
        on_change do |evt|
          owner.event "Pages on_change: #{evt.value}"
        end
        page "Page1" do
          on_select do
            owner.event "Page1 on_select"
          end
        end
        page "Page2" do
          on_select do
            owner.event "Page2 on_select"
          end
        end
        page "Page3" do
          on_select do
            owner.event "Page3 on_select"
          end
        end
      end
      label "Scroll:"
      checkbox("horiz__scroll", checked: true).on_change do |evt|
        @horiz_scroll = evt.value > 0
      end
      checkbox("vert__scroll", checked: true).on_change do |evt|
        @vert_scroll = evt.value > 0
      end
      scroll width: 300 do
        horiz_scroll @horiz_scroll
        vert_scroll @vert_scroll
        label do
          text "Scroll with horiz_scroll = #{@horiz_scroll}, vert_scroll = #{@vert_scroll}"
        end
        ellipse x: 50, y: 50, width: 300, height: 300
      end
    end
  end
end

class FlowFlavours < GUI::Fragment
  @scrollable = false
  @vertical = false
  @autowrap = false

  def gui(owner, **args)
    flow **args, childmargin: 10, vertical: true do
      label "Flow:"
      checkbox("scrollable").on_change do |evt|
        @scrollable = evt.value > 0
      end
      checkbox("vertical").on_change do |evt|
        @vertical = evt.value > 0
      end
      checkbox("autowrap").on_change do |evt|
        @autowrap = evt.value > 0
      end
      flow width: 350, border: true do
        scrollable @scrollable
        vertical @vertical
        autowrap @autowrap
        100.times { |i| button i.to_s }
      end
    end
  end
end

{% if flag?(:darwin) %}
  @[Link(framework: "OpenGL")]
{% elsif flag?(:windows) %}
  @[Link("opengl32")]
{% else %}
  @[Link("GL")]
{% end %}
lib LibGL
  GL_COLOR_BUFFER_BIT = 0x4000
  fun clearcolor = glClearColor(red : Float32, green : Float32, blue : Float32, alpha : Float32)
  fun clear = glClear(mask : UInt32)
end

class GLControls < GUI::Fragment
  def gui(owner)
    gl = glpanel(align: al(:client))
    panel align: al(:top) do
      button "Redraw" do
        on_click do
          gl.render do
            LibGL.clearcolor(0.5, 0.0, 1.0, 1.0)
            LibGL.clear(LibGL::GL_COLOR_BUFFER_BIT)
          end
        end
      end
    end
  end
end

class AlignControls < GUI::Fragment
  def gui(owner)
    panel x: 50, y: 50 do
      label "align=:none"
    end

    panel align: al(:top) do
      label "align=:top1"
    end
    panel align: al(:left) do
      label "align=:left1"
    end
    panel align: al(:right) do
      label "align=:right1"
    end

    panel align: al(:top) do
      label "align=:top2"
    end
    panel align: al(:bottom) do
      label "align=:bottom1"
    end
    panel align: al(:bottom) do
      label "align=:bottom2"
    end

    panel align: al(:left) do
      label "align=:left2"
    end
    panel align: al(:right) do
      label "align=:right2"
    end

    panel align: al(:client) do
      label "align=:client"
    end
    panel align: al(:center) do
      label "align=:center"
    end
  end
end

class MainForm < GUI::Form
  # Panel Scroll Flow GLPanel Picture
  @events = [] of String
  @basic_controls = BasicControls.new
  @basic_events = BasicEvents.new
  @shapes = Shapes.new
  @grouping_controls = GroupingControls.new
  @flow_flavours = FlowFlavours.new
  @gl_controls = GLControls.new
  @fonts_demo = FontsDemo.new
  @align_controls = AlignControls.new

  def event(s)
    @events << s
  end

  def gui
    pages align: al(:client) do
      page "Basic controls and events" do
        flow align: al(:client), childmargin: 5 do
          @basic_controls.gui self, width: 200, height: 740, border: true
          flow vertical: true, width: 300, height: 740 do
            @basic_events.gui self, width: 300, height: 400, border: true
            @shapes.gui self, width: 300, height: 300, border: true
          end
          @fonts_demo.gui self, width: 250, height: 740, border: true
        end
      end
      page "Grouping controls" do
        @grouping_controls.gui self, align: al(:left), width: 350, border: true
        @flow_flavours.gui self, align: al(:client), border: true
      end
      page "OpenGL control" do
        @gl_controls.gui self
      end
      page "Aligns system" do
        @align_controls.gui self
      end
    end
    memo do
      align :right
      width 250
      border false
      vert_scroll true
      text @events.join('\n')
      on_click do
        @events.clear
      end
    end
  end
end

GUI.run(MainForm.new(text: "GUI controls gallery", height: 768, width: 1024))
