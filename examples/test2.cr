class Form
  def form_method
    p "form_method"
  end

  def do_work(&block)
    with self yield
  end

  def subbutton(&block)
    b = SubButton.new
    b.do_work do
      with b yield
    end
  end
end

class Button < Form
  def button_method
    p "button_method"
  end

  def do_work(&block)
    with self yield
  end
end

class SubButton < Button
  def subbutton_method
    p "subbutton_method"
  end
end

Form.new.do_work do
  form_method
  subbutton do
    button_method
    subbutton_method
  end
end
