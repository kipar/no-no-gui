unit nonogui;

{$mode objfpc}{$H+}

interface

const
{$ifdef windows}
  nonoguiLIB = 'nonogui.dll';
{$else}
  nonoguiLIB = 'libnonogui.so';
{$endif}

type
  TMyComponent = Pointer;
  TMyWindow = Pointer;

  TMyAlign = (None, Client, Top, Bottom, Left, Right, Center);

  TMyEventType = (Quitting, Click, Change, MouseButton, MouseMove, MouseBorder, Keyboard);
  TEventTypes = UInt64;

  TUserCallback = record
    ptr1, ptr2: Pointer;
  end;

  TEvent = record
    UserCallback: TUserCallback;
    case typ: integer of
      -1: (Data: array[1..4] of integer);
      Change: (NewValue: Integer; NewText: PChar);
      MouseButton: (X, Y: integer;
        Button: integer;
        Down: boolean);
      MouseMove: (mX, mY: integer;
        Shift: integer);
      MouseBorder: (Entered: boolean);
      Keyboard: (Scancode: word;
        Sym: char;
        KeyDown: boolean);
  end;
  PEvent = ^TEvent;

  TMyColor = Cardinal;

function ui_create_window: TMyWindow; cdecl; external nonoguiLIB;
procedure ui_process; cdecl; external nonoguiLIB;
procedure ui_component_destroy(c: TMyComponent); cdecl; external nonoguiLIB;

procedure ui_component_set_visible(c: TMyComponent; Value: boolean);
  cdecl; external nonoguiLIB;
procedure ui_component_set_x(c: TMyComponent; Value: integer); cdecl; external nonoguiLIB;
procedure ui_component_set_y(c: TMyComponent; Value: integer); cdecl; external nonoguiLIB;
procedure ui_component_set_w(c: TMyComponent; Value: integer); cdecl; external nonoguiLIB;
procedure ui_component_set_h(c: TMyComponent; Value: integer); cdecl; external nonoguiLIB;
procedure ui_component_set_text(c: TMyComponent; Value: PChar); cdecl; external nonoguiLIB;

procedure ui_component_set_enabled(c: TMyComponent; Value: boolean);  cdecl; external nonoguiLIB;
procedure ui_component_set_align(c: TMyComponent; Value: TMyAlign);  cdecl; external nonoguiLIB;
procedure ui_component_set_parent(c: TMyComponent; Value: TMyComponent);  cdecl; external nonoguiLIB;
procedure ui_component_set_data(c: TMyComponent; Value: Pointer);  cdecl; external nonoguiLIB;
procedure ui_component_set_value(c: TMyComponent; Value: Integer);cdecl;external nonoguiLIB;
procedure ui_component_set_checked(c: TMyComponent; Value: Boolean);cdecl;external nonoguiLIB;
procedure ui_component_set_index(c: TMyComponent; Value: Integer);cdecl;external nonoguiLIB;
procedure ui_component_set_lines_count(c: TMyComponent; Value: Integer);cdecl;external nonoguiLIB;
procedure ui_component_set_line(c: TMyComponent; Index: Integer; Value: PChar);cdecl;external nonoguiLIB;
function ui_component_get_text(c: TMyComponent): PChar;cdecl;external nonoguiLIB;
function ui_component_get_value(c: TMyComponent): Integer;cdecl;external nonoguiLIB;
function ui_component_get_index(c: TMyComponent): Integer;cdecl;external nonoguiLIB;
function ui_component_get_checked(c: TMyComponent): Boolean;cdecl;external nonoguiLIB;
function ui_glpanel_get_handle(c: TMyComponent): Pointer;cdecl;external nonoguiLIB;
procedure ui_glpanel_swap_buffers(c: TMyComponent); cdecl;external nonoguiLIB;
procedure ui_glpanel_select(c: TMyComponent); cdecl;external nonoguiLIB;
procedure ui_component_set_horizscroll(c: TMyComponent; Value: Boolean); cdecl;external nonoguiLIB;
procedure ui_component_set_vertscroll(c: TMyComponent; Value: Boolean); cdecl;external nonoguiLIB;
procedure ui_component_set_vertical(c: TMyComponent; Value: boolean); cdecl;external nonoguiLIB;
procedure ui_component_set_pencolor(c: TMyComponent; Color: TMyColor); cdecl;external nonoguiLIB;
procedure ui_component_set_brushcolor(c: TMyComponent; Color: TMyColor); cdecl;external nonoguiLIB;
procedure ui_flow_set_scrollable(c: TMyComponent; Value: Boolean); cdecl;external nonoguiLIB;
procedure ui_component_set_autowrap(c: TMyComponent; Value: boolean); cdecl;external nonoguiLIB;
procedure ui_component_set_border(c: TMyComponent; Value: Boolean); cdecl;external nonoguiLIB;

procedure ui_component_set_handler(c: TMyComponent; mask: TEventTypes;  Data: TUserCallback); cdecl; external nonoguiLIB;
function ui_events_count: Integer; cdecl; external nonoguiLIB;
function ui_event_get(index: Integer): PEvent; cdecl; external nonoguiLIB;


function ui_create_button(form: TMyWindow): TMyComponent; cdecl; external nonoguiLIB;
function ui_create_label(form: TMyWindow): TMyComponent; cdecl; external nonoguiLIB;
function ui_create_edit(form: TMyWindow): TMyComponent; cdecl; external nonoguiLIB;
function ui_create_slider(form: TMyWindow): TMyComponent;cdecl; external nonoguiLIB;
function ui_create_progressbar(form: TMyWindow): TMyComponent;cdecl; external nonoguiLIB;
function ui_create_checkbox(form: TMyWindow): TMyComponent;cdecl; external nonoguiLIB;
function ui_create_memo(form: TMyWindow): TMyComponent;cdecl; external nonoguiLIB;
function ui_create_listbox(form: TMyWindow): TMyComponent;cdecl; external nonoguiLIB;
function ui_create_combobox(form: TMyWindow): TMyComponent;cdecl; external nonoguiLIB;
function ui_create_glpanel(form: TMyWindow): TMyComponent;cdecl; external nonoguiLIB;

function ui_create_panel(form: TMyWindow): TMyComponent; cdecl; external nonoguiLIB;
function ui_create_scroll(form: TMyWindow): TMyComponent; cdecl; external nonoguiLIB;
function ui_create_flow(form: TMyWindow): TMyComponent; cdecl; external nonoguiLIB;
function ui_create_pages(form: TMyWindow): TMyComponent;cdecl; external nonoguiLIB;

function ui_pages_get_page(c: TMyComponent; index: Integer): TMyComponent;cdecl; external nonoguiLIB;
procedure ui_pages_delete_page(c: TMyComponent; index: Integer);cdecl; external nonoguiLIB;
function ui_pages_add_page(c: TMyComponent; caption: PChar): TMyComponent;cdecl; external nonoguiLIB;

implementation

end.
