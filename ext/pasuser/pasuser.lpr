program pasuser;

uses nonogui, sysutils;

var
  w: TMyWindow;
  gl, flw, btn, btn2, edt, lbl: TMyComponent;
  shouldquit: boolean = false;
  counter: Integer = 0;
  i: Integer;
  handler: TUserCallback;



begin

  w := ui_create_window;
  ui_component_set_visible(w, true);

  btn := ui_create_button(w);
  ui_component_set_align(btn, Top);
  ui_component_set_text(btn, 'Привет, мир!');
  handler.ptr1 := btn;
  ui_component_set_handler(btn, 1 shl ord(TMyEventType.Click),handler);
  ui_component_set_parent(btn, w);

  edt := ui_create_edit(w);
  ui_component_set_align(edt, Top);
  handler.ptr1 := edt;
  ui_component_set_text(edt, 'Привет, мир!');
  ui_component_set_handler(edt, 1 shl ord(TMyEventType.Change),handler);
  ui_component_set_parent(edt, w);

  lbl := ui_create_label(w);
  ui_component_set_align(lbl, Top);
  ui_component_set_text(lbl, 'Привет, мир!');
  ui_component_set_parent(lbl, w);

  flw := ui_create_flow(w);
  ui_component_set_align(flw, Client);
  ui_flow_set_scrollable(flw, true);
  ui_component_set_vertical(flw, true);
  ui_component_set_parent(flw, w);
  for i := 1 to 1000 do
  begin
    btn2 := ui_create_button(w);
    ui_component_set_text(btn2, PChar(IntToStr(i)));
    ui_component_set_parent(btn2, flw);
  end;

  gl := ui_create_glpanel(w);
  ui_component_set_parent(gl, w);


  repeat
    counter := counter+1;
    ui_component_set_text(btn, PChar('Привет, мир! '+InttoStr(counter)));
    sleep(0);
    ui_process;
    for i := 0 to ui_events_count - 1 do
      case TMyEventType(ui_event_get(i)^.typ) of
        //TMyEventType.Click: shouldquit := True;
        TMyEventType.Quitting: shouldquit := True;
        else
          writeln(TMyEventType(ui_event_get(i)^.typ));
      end;
  until shouldquit;
end.

