unit uImages;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uTypes, Graphics, ExtCtrls;

function ui_load_images(Imagedir: PChar; md5hash: PChar): boolean; cdecl;
function ui_create_picture(form: TMyWindow): TMyComponent; cdecl;
//function ui_create_glyph(form: TMyWindow): TMyComponent; cdecl;
//function ui_create_custombutton(form: TMyWindow): TMyComponent; cdecl;
//function ui_create_customtoggle(form: TMyWindow): TMyComponent; cdecl;
procedure ui_set_image(c: TMyComponent; img: TMyImage); cdecl;

function hash_init: Pointer; cdecl;
procedure hash_update(hash: pointer; data: PChar; size: Integer); cdecl;
function hash_size(hash: pointer): Integer; cdecl;
procedure hash_final(hash: pointer; result: PChar); cdecl;

var
  LOADED_IMAGES: array of TPicture;

implementation

uses md5;

const
  SLASH = '/';

var
  Counting: boolean;
  Counter: integer = 0;
  Hash: Pointer;
  BaseDir: string;


procedure ProcessFile(s: string);
begin
  //writeln('processing file: '+s);
  if Counting then
  begin
    Inc(Counter);
    hash_update(Hash, PChar(s), length(s));
  end
  else
  begin
    //writeln(IntToStr(Counter)+': loading '+BaseDir + s);
    LOADED_IMAGES[Counter] := TPicture.Create;
    LOADED_IMAGES[Counter].LoadFromFile(BaseDir + s);
    Inc(Counter);
  end;
end;

procedure ProcessDir(s: string);
var
  ff: TSearchRec;
  list: TStringList;
  item: string;
begin
  //writeln('processing dir: '+s);
  if FindFirst(BaseDir + s + '*', faAnyFile, ff) <> 0 then
      exit;
  list := TStringList.Create;
  repeat
    if (ff.Name = '.') or (ff.name = '..') then
      continue;
    if DirectoryExists(BaseDir + s + ff.Name) then
      list.add(ff.Name);
  until FindNext(ff) <> 0;
  FindClose(ff);
  list.sort;
  for item in list do
    ProcessDir(s + item + SLASH);
  list.clear;
  FindFirst(BaseDir + s + '*.*', faAnyFile - faDirectory, ff);
  repeat
    if not DirectoryExists(BaseDir + s + ff.Name) and FileExists(BaseDir + s + ff.Name) then
      list.add(ff.name);
  until FindNext(ff) <> 0;
  FindClose(ff);
  list.sort;
  for item in list do
    ProcessFile(s + item);
  list.Free;
  //writeln('processing dir complete: '+s);
end;

function ui_load_images(Imagedir: PChar; md5hash: PChar): boolean; cdecl;
var
  s: string;
begin
  //writeln('loading started '+Imagedir);
  Counting := True;
  Counter := 0;
  BaseDir := Imagedir + SLASH;
  Hash := hash_init;
  ProcessDir('');
  SetLength(s, hash_size(Hash));
  hash_final(Hash, PChar(s));
  Hash := nil;
  Result := (md5hash = 'IGNORE') or (md5hash = s);
  if Result then
  begin
    SetLength(LOADED_IMAGES, Counter);
    Counting := False;
    Counter := 0;
    ProcessDir('');
  end;
  //writeln('loading complete');
end;

function ui_create_picture(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TImage.Create(TComponent(Form)));
  TImage(Result).Stretch := True;
end;

procedure ui_set_image(c: TMyComponent; img: TMyImage); cdecl;
begin
  if TObject(c) is TImage then
    TImage(c).Picture := LOADED_IMAGES[img];
end;

function hash_init: Pointer; cdecl;
var
  MDContext: PMDContext;
begin
  New(MDContext);
  MD5Init(MDContext^);
  Result := MDContext;
end;

procedure hash_update(hash: pointer; data: PChar; size: Integer); cdecl;
begin
  MD5Update(PMDContext(hash)^, data^, size);
end;

function hash_size(hash: pointer): Integer; cdecl;
begin
  Result := 32;
end;

procedure hash_final(hash: pointer; result: PChar); cdecl;
var
  MD5Digest: TMDDigest;
  s: string;
begin
  MD5Final(PMDContext(hash)^, MD5Digest);
  s := MD5Print(MD5Digest);
  Move(Pointer(s)^, result^, hash_size(hash));
  Dispose(PMDContext(hash));
end;


var
  i: integer;
finalization
  for i := 0 to Length(LOADED_IMAGES) - 1 do
    LOADED_IMAGES[i].Free;
end.
