unit uEvents;

{$mode objfpc}{$H+}

interface

uses uTypes, fgl, Classes, Controls, StdCtrls, ComCtrls, ExtCtrls, Forms;

function ui_events_count: integer; cdecl;
function ui_event_get(index: integer): PEvent; cdecl;

type

  TFlowProperties = record
    Vertical: Boolean;
    Scrollable: Boolean;
    AutoWrap: Boolean;
  end;

  TTagObject = class
    UserData: pointer;
    ChildMargin: integer;
    FlowProperties: TFlowProperties;
    EventHandlers: array[TEventType] of TUserCallback;
  end;

  TEventList = TFPSList;

  { TAllEvents }

  TAllEvents = class
    Data: TEventList;

    procedure AddHandler(c: TMyComponent; mask: TEventTypes; handler: TUserCallback);

    procedure CatchOnClick(Sender: TObject);
    procedure CatchOnMouseEnter(Sender: TObject);
    procedure CatchOnMouseLeave(Sender: TObject);
    procedure CatchOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure CatchOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure CatchOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure CatchOnKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure CatchOnKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);

    procedure CatchOnSelectionChangeListBox(Sender: TObject; User: boolean);
    procedure CatchOnChangeCheckBox(Sender: TObject);
    procedure CatchOnChangeComboBox(Sender: TObject);
    procedure CatchOnChangeMemo(Sender: TObject);
    procedure CatchOnChangeTrackBar(Sender: TObject);
    procedure CatchOnChangePageControl(Sender: TObject);
    procedure CatchOnChangeEdit(Sender: TObject);
    procedure CatchOnPageSelect(Sender: TObject);

    constructor Create;

    procedure Reset;
    procedure QuitHappened;

  private
    function Happened(c: TMyComponent; what: TEventType): PEvent;
  end;

var
  AllEvents: TAllEvents;

implementation

type
  TOpenControl = class(TWinControl)
    property OnMouseDown;
    property OnMouseUp;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
  end;




function ui_events_count: integer; cdecl;
begin
  Result := AllEvents.Data.Count;
end;

function ui_event_get(index: integer): PEvent; cdecl;
begin
  if (index >= 0) and (index < AllEvents.Data.Count) then
    Result := AllEvents.Data[index]
  else
    Result := nil;
end;

{ TAllEvents }

procedure TAllEvents.AddHandler(c: TMyComponent; mask: TEventTypes; handler: TUserCallback);
var
  typ: TEventType;
begin
  if not assigned(TTagObject(TControl(c).Tag)) then
    TControl(c).Tag := PtrInt(TTagObject.Create);
  for typ in TEventType do
    if ((1 shl Ord(typ)) and mask) <> 0 then
    begin
      TTagObject(TControl(c).Tag).EventHandlers[typ] := handler;
      case typ of
        Click: TControl(c).OnClick := @AllEvents.CatchOnClick;
        MouseButton:
        begin
          TOpenControl(c).OnMouseDown := @AllEvents.CatchOnMouseDown;
          TOpenControl(c).OnMouseUp := @AllEvents.CatchOnMouseUp;
        end;
        MouseMove:
          TOpenControl(c).OnMouseMove := @AllEvents.CatchOnMouseMove;
        MouseBorder:
        begin
          TOpenControl(c).OnMouseEnter := @AllEvents.CatchOnMouseEnter;
          TOpenControl(c).OnMouseLeave := @AllEvents.CatchOnMouseLeave;
        end;
        Keyboard:
        begin
          TOpenControl(c).OnKeyDown := @AllEvents.CatchOnKeyDown;
          TOpenControl(c).OnKeyUp := @AllEvents.CatchOnKeyUp;
        end;
        Change:
        begin
          if TControl(c) is TCheckBox then
            TCheckBox(c).OnChange := @AllEvents.CatchOnChangeCheckBox
          else if TControl(c) is TComboBox then
          begin
            TComboBox(c).OnChange := @AllEvents.CatchOnChangeComboBox;
            //TComboBox(c).OnSelect := @AllEvents.CatchOnChangeComboBox
          end
          else if TControl(c) is TListBox then
            TListBox(c).OnSelectionChange := @AllEvents.CatchOnSelectionChangeListBox
          else if TControl(c) is TMemo then
            TMemo(c).OnChange := @AllEvents.CatchOnChangeMemo
          else if TControl(c) is TTrackBar then
            TTrackBar(c).OnChange := @AllEvents.CatchOnChangeTrackBar
          else if TControl(c) is TPageControl then
            TPageControl(c).OnChange := @AllEvents.CatchOnChangePageControl
          else if TControl(c) is TEdit then
            TEdit(c).OnChange := @AllEvents.CatchOnChangeEdit
          else if TControl(c) is TTabSheet then
            TTabSheet(c).OnShow := @AllEvents.CatchOnPageSelect;
        end;
      end;
    end;
end;

procedure TAllEvents.CatchOnClick(Sender: TObject);
begin
  Happened(TMyComponent(Sender), Click);
end;

procedure TAllEvents.CatchOnMouseEnter(Sender: TObject);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), MouseBorder);
  if Assigned(ev) then
    ev^.Entered := True;
end;

procedure TAllEvents.CatchOnMouseLeave(Sender: TObject);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), MouseBorder);
  if Assigned(ev) then
    ev^.Entered := False;
end;

procedure TAllEvents.CatchOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), MouseButton);
  if Assigned(ev) then
  begin
    ev^.X := X;
    ev^.Y := Y;
    ev^.Button := Ord(Button);
    ev^.Down := False;
  end;
end;

procedure TAllEvents.CatchOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), MouseButton);
  if Assigned(ev) then
  begin
    ev^.X := X;
    ev^.Y := Y;
    ev^.Button := Ord(Button);
    ev^.Down := True;
  end;
end;

procedure TAllEvents.CatchOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), MouseMove);
  if Assigned(ev) then
  begin
    ev^.mX := X;
    ev^.mY := Y;
  end;
end;

procedure TAllEvents.CatchOnKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), Keyboard);
  if Assigned(ev) then
  begin
    ev^.Scancode := Key;
    //ev^.Sym := Chr(Key);
    ev^.KeyDown := False;
  end;
end;

procedure TAllEvents.CatchOnKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), Keyboard);
  if Assigned(ev) then
  begin
    ev^.Scancode := Key;
    //ev^.Sym := Chr(Key);
    ev^.KeyDown := True;
  end;
end;

procedure TAllEvents.CatchOnSelectionChangeListBox(Sender: TObject; User: boolean);
var
  ev: PEvent;
begin
  if not User then
    exit;
  ev := Happened(TMyComponent(Sender), Change);
  if Assigned(ev) then
    ev^.NewValue := TListBox(Sender).ItemIndex;
end;

procedure TAllEvents.CatchOnChangeCheckBox(Sender: TObject);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), Change);
  if Assigned(ev) then
    ev^.NewValue := Ord(TCheckBox(Sender).Checked);
end;

procedure TAllEvents.CatchOnChangeComboBox(Sender: TObject);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), Change);
  if Assigned(ev) then
  begin
    ev^.NewValue := TComboBox(Sender).ItemIndex;
    ev^.NewText := PChar(TComboBox(Sender).Text);
  end;
end;

procedure TAllEvents.CatchOnChangeMemo(Sender: TObject);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), Change);
  if Assigned(ev) then
    ev^.NewText := PChar(TMemo(Sender).Text);
end;

procedure TAllEvents.CatchOnChangeTrackBar(Sender: TObject);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), Change);
  if Assigned(ev) then
    ev^.NewValue := TTrackBar(Sender).Position;
end;

procedure TAllEvents.CatchOnChangePageControl(Sender: TObject);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), Change);
  if Assigned(ev) then
    ev^.NewValue := TPageControl(Sender).ActivePageIndex;
end;

procedure TAllEvents.CatchOnChangeEdit(Sender: TObject);
var
  ev: PEvent;
begin
  ev := Happened(TMyComponent(Sender), Change);
  if Assigned(ev) then
    ev^.NewText := PChar(TEdit(Sender).Text);
end;

procedure TAllEvents.CatchOnPageSelect(Sender: TObject);
begin
  Happened(TMyComponent(Sender), Change);
end;

constructor TAllEvents.Create;
begin
  Data := TFPSList.Create(sizeof(TEvent));
end;

procedure TAllEvents.Reset;
begin
  Data.Count := 0;
end;

procedure TAllEvents.QuitHappened;
var
  evt: TEvent;
begin
  evt.typ := Ord(Quitting);
  Data.Add(@evt);
end;

function TAllEvents.Happened(c: TMyComponent; what: TEventType): PEvent;
var
  tag: TTagObject;
  evt: TEvent;
begin
  Result := nil;
  tag := TTagObject(TControl(c).Tag);
  if not assigned(tag) then
    exit;
  if not assigned(tag.EventHandlers[what].ptr1) then
    exit;
  evt.typ := Ord(what);
  evt.UserCallback := tag.EventHandlers[what];
  Result := Data[Data.Add(@evt)];
end;

end.
