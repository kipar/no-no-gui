unit uTypes;

{$mode objfpc}{$H+}

interface

type
  TMyComponent = Pointer;
  TMyWindow = Pointer;

  TMyAlign = (None, Client, Top, Bottom, Left, Right, Center);
  TMyMouseEvent = (Enter, Leave, Down, Up);

  TEventType = (Quitting, Click, Change, MouseButton, MouseMove, MouseBorder, Keyboard);
  TEventTypes = UInt64;

  TUserCallback = packed record
    ptr1, ptr2: Pointer;
  end;

  TEvent = packed record
    UserCallback: TUserCallback;
    case typ: integer of
      -1: (Data: array[1..4] of integer);
      Change: (NewValue: integer;
        NewText: PChar);
      MouseButton: (X, Y: integer;
        Button: integer;
        Down: boolean);
      MouseMove: (mX, mY: integer;
        Shift: integer);
      MouseBorder: (Entered: boolean);
      Keyboard: (Scancode: Integer;
        //Sym: Char;
        KeyDown: boolean);
  end;
  PEvent = ^TEvent;

  TMyShapeType = (Ellipse, Rect, TriaUp, TriaDown, TriaLeft, TriaRight, Diamond);
  TMyColor = cardinal;
  TMyImage = cardinal;

implementation

end.
