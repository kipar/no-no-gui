unit uWindow;

{$mode objfpc}{$H+}

interface

uses
  Classes, Interfaces, Forms, uTypes;

function ui_create_window: TMyWindow; cdecl;
procedure ui_process; cdecl;
function ui_window_get_visible(w: TMyWindow): boolean; cdecl;
procedure ui_window_set_fullscreen(w: TMyWindow; Value: boolean); cdecl;

procedure InitAll;

var
  fff: TForm;

implementation

uses uEvents, uComponent;

procedure InitAll;
begin
  AllEvents := TAllEvents.Create;
  flows_to_process := TList.Create;
end;

function ui_create_window: TMyWindow; cdecl;
var
  aForm: TForm;
begin
  if not (AppInitialized in Application.Flags) then
  begin
    InitAll;
    Application.Initialize;
  end;
  Application.CreateForm(TForm, aForm);
  Result := TMyWindow(aForm);
  if fff = nil then
  begin
    fff := aForm;
    fff.DisableAlign;
    fff.DisableAutoSizing;
  end;
end;

procedure ui_process; cdecl;
var
  c: Pointer;
begin
  for c in flows_to_process do
    flow_manual_scroll(TMyComponent(c));
  flows_to_process.Clear;


  fff.EnableAlign;
  fff.EnableAutoSizing;
  AllEvents.Reset;

  Application.ProcessMessages;
  if Application.Terminated then
    AllEvents.QuitHappened;

  fff.DisableAlign;
  fff.DisableAutoSizing;
end;

function ui_window_get_visible(w: TMyWindow): boolean; cdecl;
begin
  Result := TForm(w).Visible;
end;

procedure ui_window_set_fullscreen(w: TMyWindow; Value: boolean); cdecl;
begin
  if Value then
    TForm(w).WindowState := wsFullScreen
  else
    TForm(w).WindowState := wsNormal;
end;

end.
