unit uComponent;

{$mode objfpc}{$H+}

interface

uses
  Classes, Interfaces, Forms, Controls, Graphics, StdCtrls, ComCtrls, ExtCtrls, OpenGLContext, uTypes, uEvents, Math;

procedure ui_component_destroy(c: TMyComponent); cdecl;

procedure ui_component_set_visible(c: TMyComponent; Value: boolean); cdecl;
procedure ui_component_set_x(c: TMyComponent; Value: integer); cdecl;
procedure ui_component_set_y(c: TMyComponent; Value: integer); cdecl;
procedure ui_component_set_w(c: TMyComponent; Value: integer); cdecl;
procedure ui_component_set_h(c: TMyComponent; Value: integer); cdecl;
function ui_component_get_x(c: TMyComponent): integer; cdecl;
function ui_component_get_y(c: TMyComponent): integer; cdecl;
function ui_component_get_w(c: TMyComponent): integer; cdecl;
function ui_component_get_h(c: TMyComponent): integer; cdecl;

procedure ui_component_set_text(c: TMyComponent; Value: PChar); cdecl;
procedure ui_component_set_enabled(c: TMyComponent; Value: boolean); cdecl;
procedure ui_component_set_align(c: TMyComponent; Value: TMyAlign); cdecl;
procedure ui_component_set_parent(c: TMyComponent; Value: TMyComponent); cdecl;
procedure ui_component_set_data(c: TMyComponent; Value: Pointer); cdecl;
procedure ui_component_set_value(c: TMyComponent; Value: integer); cdecl;
procedure ui_component_set_checked(c: TMyComponent; Value: boolean); cdecl;
procedure ui_component_set_index(c: TMyComponent; Value: integer); cdecl;
procedure ui_component_set_lines_count(c: TMyComponent; Value: integer); cdecl;
procedure ui_component_set_line(c: TMyComponent; Index: integer; Value: PChar); cdecl;
procedure ui_component_set_all_lines(c: TMyComponent; Value: PChar); cdecl;
function ui_component_get_text(c: TMyComponent): PChar; cdecl;
function ui_component_get_value(c: TMyComponent): integer; cdecl;
function ui_component_get_index(c: TMyComponent): integer; cdecl;
function ui_component_get_checked(c: TMyComponent): boolean; cdecl;
function ui_glpanel_get_handle(c: TMyComponent): Pointer; cdecl;
procedure ui_glpanel_swap_buffers(c: TMyComponent); cdecl;
procedure ui_glpanel_select(c: TMyComponent); cdecl;
procedure ui_component_set_horizscroll(c: TMyComponent; Value: boolean); cdecl;
procedure ui_component_set_vertscroll(c: TMyComponent; Value: boolean); cdecl;
procedure ui_component_set_vertical(c: TMyComponent; Value: boolean); cdecl;
procedure ui_component_set_pencolor(c: TMyComponent; Color: TMyColor); cdecl;
procedure ui_component_set_brushcolor(c: TMyComponent; Color: TMyColor); cdecl;
procedure ui_flow_set_scrollable(c: TMyComponent; Value: boolean); cdecl;
procedure ui_component_set_autowrap(c: TMyComponent; Value: boolean); cdecl;
procedure ui_component_set_margin(c: TMyComponent; Value: integer); cdecl;
procedure ui_component_set_childmargin(c: TMyComponent; Value: integer); cdecl;
procedure ui_component_set_border(c: TMyComponent; Value: boolean); cdecl;

procedure ui_component_set_handler(c: TMyComponent; mask: TEventTypes; Data: TUserCallback); cdecl;

function ui_create_button(form: TMyWindow): TMyComponent; cdecl;
function ui_create_label(form: TMyWindow): TMyComponent; cdecl;
function ui_create_edit(form: TMyWindow): TMyComponent; cdecl;
function ui_create_slider(form: TMyWindow): TMyComponent; cdecl;
function ui_create_progressbar(form: TMyWindow): TMyComponent; cdecl;
function ui_create_checkbox(form: TMyWindow): TMyComponent; cdecl;
function ui_create_memo(form: TMyWindow): TMyComponent; cdecl;
function ui_create_listbox(form: TMyWindow): TMyComponent; cdecl;
function ui_create_combobox(form: TMyWindow): TMyComponent; cdecl;
function ui_create_glpanel(form: TMyWindow): TMyComponent; cdecl;
function ui_create_shape(form: TMyWindow; typ: TMyShapeType): TMyComponent; cdecl;
function ui_create_flow_wrap(form: TMyWindow): TMyComponent; cdecl;

function ui_create_panel(form: TMyWindow): TMyComponent; cdecl;
function ui_create_scroll(form: TMyWindow): TMyComponent; cdecl;
function ui_create_flow(form: TMyWindow): TMyComponent; cdecl;
function ui_create_pages(form: TMyWindow): TMyComponent; cdecl;
function ui_pages_add_page(c: TMyComponent; Caption: PChar): TMyComponent; cdecl;

procedure flow_apply_properties(c: TMyComponent);
procedure flow_manual_scroll(c: TMyComponent);
procedure flow_request_manual_scroll(c: TMyComponent);


procedure ui_component_set_font_name(c: TMyComponent; FontName: PChar); cdecl;
procedure ui_component_set_font_size(c: TMyComponent; FontSize: Integer); cdecl;
procedure ui_component_set_font_color(c: TMyComponent; FontColor: TMyColor); cdecl;
procedure ui_component_set_font_style(c: TMyComponent; Style: Integer); cdecl;

var
  flows_to_process: TList;

implementation

function IsFlow(c: TMyComponent): boolean;
begin
  Result := (TControl(c) is TScrollBox) and (TWinControl(c).ControlCount = 1) and (TWinControl(c).Controls[0] is TFlowPanel);
end;



procedure ui_component_destroy(c: TMyComponent); cdecl;
var
  par: TControl;
begin
  par := TControl(c).Parent;
  TTagObject(TControl(c).Tag).Free;
  TObject(c).Free;
  if par is TFlowPanel then
    flow_request_manual_scroll(par.Parent);
end;

procedure ui_component_set_visible(c: TMyComponent; Value: boolean); cdecl;
begin
  if (TControl(c).Parent <> nil) and (TControl(c).Parent is TFlowPanel) then
    flow_request_manual_scroll(TControl(c).Parent.Parent);
  TControl(c).Visible := Value;
end;

procedure ui_component_set_x(c: TMyComponent; Value: integer); cdecl;
begin
  TControl(c).Left := Value;
  TControl(c).BorderSpacing.Left := Value;
  TControl(c).BorderSpacing.Right := Value;
end;

procedure ui_component_set_y(c: TMyComponent; Value: integer); cdecl;
begin
  TControl(c).Top := Value;
  TControl(c).BorderSpacing.Top := Value;
  TControl(c).BorderSpacing.Bottom := Value;
end;

procedure ui_component_set_w(c: TMyComponent; Value: integer); cdecl;
begin
  TControl(c).Width := Value;
  TControl(c).Constraints.MaxWidth := Value;
end;

procedure ui_component_set_h(c: TMyComponent; Value: integer); cdecl;
begin
  TControl(c).Height := Value;
  TControl(c).Constraints.MaxHeight := Value;
end;

function ui_component_get_x(c: TMyComponent): integer; cdecl;
begin
  Result := TControl(c).Left;
end;

function ui_component_get_y(c: TMyComponent): integer; cdecl;
begin
  Result := TControl(c).Top;
end;

function ui_component_get_w(c: TMyComponent): integer; cdecl;
begin
  Result := TControl(c).Width;
end;

function ui_component_get_h(c: TMyComponent): integer; cdecl;
begin
  Result := TControl(c).Height;
end;

procedure ui_component_set_text(c: TMyComponent; Value: PChar); cdecl;
begin
  if TControl(c) is TEdit then
    TEdit(c).Text := Value
  else if TControl(c) is TComboBox then
    TComboBox(c).Text := Value
  else if TControl(c) is TListBox then
    TListBox(c).Items.Text := Value
  else if TControl(c) is TMemo then
    TMemo(c).Lines.Text := Value
  else
    TControl(c).Caption := Value;
end;

procedure ui_component_set_enabled(c: TMyComponent; Value: boolean); cdecl;
begin
  TControl(c).Enabled := Value;
end;

procedure ui_component_set_align(c: TMyComponent; Value: TMyAlign); cdecl;
begin
  case Value of
    None: TControl(c).Align := alNone;
    Client: TControl(c).Align := alClient;
    Top: TControl(c).Align := alTop;
    Bottom: TControl(c).Align := alBottom;
    Left: TControl(c).Align := alLeft;
    Right: TControl(c).Align := alRight;
    Center:
    begin
      TControl(c).Align := alNone;
      TControl(c).AnchorSide[akTop].Side := asrCenter;
      TControl(c).AnchorSide[akLeft].Side := asrCenter;
    end;
  end;
end;

function IsFlowWrap(c: TMyComponent): boolean;
begin
  Result := (TControl(c) is TShape) and (TControl(c).Width = 0);
end;

procedure ui_component_set_parent(c: TMyComponent; Value: TMyComponent); cdecl;
begin
  if IsFlow(Value) then
    TControl(c).Parent := TWinControl(TWinControl(Value).Controls[0])
  else
    TControl(c).Parent := TWinControl(Value);

  if (Value <> nil) and (TControl(Value).Tag <> 0) then
    TControl(c).BorderSpacing.Around := TTagObject(TControl(Value).Tag).ChildMargin;
  if IsFlowWrap(c) and (Value <> nil) and (TControl(c).Parent is TFlowPanel) then
    TFlowPanel(TControl(c).Parent).ControlList.Items[TFlowPanel(TControl(c).Parent).ControlList.Count - 1].WrapAfter := waForce;
  if (Value <> nil) and (TControl(c).Parent is TFlowPanel) then
    flow_request_manual_scroll(Value);
end;

procedure ui_component_set_data(c: TMyComponent; Value: Pointer); cdecl;
begin
  if not assigned(TTagObject(TControl(c).Tag)) then
    TControl(c).Tag := PtrInt(TTagObject.Create);
  TTagObject(TControl(c).Tag).UserData := Value;
end;

procedure ui_component_set_value(c: TMyComponent; Value: integer); cdecl;
begin
  if TControl(c) is TProgressBar then
    TProgressBar(c).Position := Value
  else if TControl(c) is TTrackBar then
    TTrackBar(c).Position := Value;
end;

procedure ui_component_set_checked(c: TMyComponent; Value: boolean); cdecl;
begin
  if TControl(c) is TCheckBox then
    TCheckBox(c).Checked := Value
  else if TControl(c) is TRadioButton then
    TRadioButton(c).Checked := Value;
end;

procedure ui_component_set_index(c: TMyComponent; Value: integer); cdecl;
begin
  if TControl(c) is TListBox then
    TListBox(c).ItemIndex := Value
  else if TControl(c) is TComboBox then
    TComboBox(c).ItemIndex := Value
  else if TControl(c) is TPageControl then
    TPageControl(c).TabIndex := Value;
end;

procedure ui_component_set_lines_count(c: TMyComponent; Value: integer); cdecl;
var
  i: integer;
  Lines: TStrings;
begin
  if TControl(c) is TListBox then
    Lines := TListBox(c).Items
  else if TControl(c) is TComboBox then
    Lines := TComboBox(c).Items
  else if TControl(c) is TMemo then
    Lines := TMemo(c).Lines
  else
    exit;
  if Lines.Count = Value then
    exit;
  Lines.BeginUpdate;
  if Lines.Count < Value then
  begin
    for i := 0 to Value - Lines.Count do
      Lines.Add('');
  end
  else
  begin
    for i := 0 to Lines.Count - Value do
      Lines.Delete(Lines.Count - 1);
  end;
  Lines.EndUpdate;
end;

procedure ui_component_set_line(c: TMyComponent; Index: integer; Value: PChar);
  cdecl;
var
  Lines: TStrings;
begin
  if TControl(c) is TListBox then
    Lines := TListBox(c).Items
  else if TControl(c) is TComboBox then
    Lines := TComboBox(c).Items
  else if TControl(c) is TMemo then
    Lines := TMemo(c).Lines
  else
    exit;
  Lines.Strings[Index] := Value;
end;

procedure ui_component_set_all_lines(c: TMyComponent; Value: PChar); cdecl;
var
  Lines: TStrings;
begin
  if TControl(c) is TListBox then
    Lines := TListBox(c).Items
  else if TControl(c) is TComboBox then
    Lines := TComboBox(c).Items
  else if TControl(c) is TMemo then
    Lines := TMemo(c).Lines
  else
    exit;
  Lines.Text := Value;
end;

function ui_component_get_text(c: TMyComponent): PChar; cdecl;
begin
  if TControl(c) is TEdit then
    Result := PChar(TEdit(c).Text)
  else if TControl(c) is TComboBox then
    Result := PChar(TComboBox(c).Text)
  else
    Result := PChar(TControl(c).Caption);
end;

function ui_component_get_value(c: TMyComponent): integer; cdecl;
begin
  if TControl(c) is TProgressBar then
    Result := TProgressBar(c).Position
  else if TControl(c) is TTrackBar then
    Result := TTrackBar(c).Position
  else
    Result := -1;
end;

function ui_component_get_index(c: TMyComponent): integer; cdecl;
begin
  if TControl(c) is TListBox then
    Result := TListBox(c).ItemIndex
  else if TControl(c) is TComboBox then
    Result := TComboBox(c).ItemIndex
  else if TControl(c) is TPageControl then
    Result := TPageControl(c).TabIndex
  else
    Result := -1;
end;

function ui_component_get_checked(c: TMyComponent): boolean; cdecl;
begin
  if TControl(c) is TCheckBox then
    Result := TCheckBox(c).Checked
  else if TControl(c) is TRadioButton then
    Result := TRadioButton(c).Checked
  else
    Result := False;
end;

function ui_glpanel_get_handle(c: TMyComponent): Pointer; cdecl;
begin
  Result := Pointer(TOpenGLControl(c).Handle);
end;

procedure memo_set_scroll(c: TMemo; vertical: boolean; Value: boolean);
var
  v, h: boolean;
begin
  case c.ScrollBars of
    ssAutoVertical:
    begin
      v := True;
      h := False;
    end;
    ssAutoHorizontal:
    begin
      v := False;
      h := True;
    end;
    ssAutoBoth:
    begin
      v := True;
      h := True;
    end;
      //    ssNone:
    else
    begin
      v := False;
      h := False;
    end;
  end;
  if vertical then
    v := Value
  else
    h := Value;
  if v then
  begin
    if h then
      c.ScrollBars := ssAutoBoth
    else
      c.ScrollBars := ssAutoVertical;
  end
  else
  begin
    if h then
      c.ScrollBars := ssAutoHorizontal
    else
      c.ScrollBars := ssNone;
  end;
end;

procedure ui_glpanel_swap_buffers(c: TMyComponent); cdecl;
begin
  TOpenGLControl(c).SwapBuffers;
end;

procedure ui_glpanel_select(c: TMyComponent); cdecl;
begin
  TOpenGLControl(c).MakeCurrent;
end;

procedure ui_component_set_horizscroll(c: TMyComponent; Value: boolean);
  cdecl;
begin
  if Isflow(c) then
    exit;
  if TControl(c) is TScrollingWinControl then
    TScrollingWinControl(c).HorzScrollBar.Visible := Value
  else if TControl(c) is TMemo then
    memo_set_scroll(TMemo(c), False, Value);
end;

procedure ui_component_set_vertscroll(c: TMyComponent; Value: boolean);
  cdecl;
begin
  if Isflow(c) then
    exit;
  if TControl(c) is TScrollingWinControl then
    TScrollingWinControl(c).VertScrollBar.Visible := Value
  else if TControl(c) is TMemo then
    memo_set_scroll(TMemo(c), True, Value);
end;

procedure ui_component_set_vertical(c: TMyComponent; Value: boolean); cdecl;
begin
  if Isflow(c) then
  begin
    TTagObject(TControl(c).Tag).FlowProperties.Vertical := Value;
    flow_apply_properties(c);
  end
  else if TControl(c) is TTrackBar then
  begin
    if Value then
      TTrackBar(c).Orientation := trVertical
    else
      TTrackBar(c).Orientation := trHorizontal;
  end
  else if TControl(c) is TProgressBar then
  begin
    if Value then
      TProgressBar(c).Orientation := pbVertical
    else
      TProgressBar(c).Orientation := pbHorizontal;
  end;
end;

procedure ui_component_set_pencolor(c: TMyComponent; Color: TMyColor); cdecl;
begin
  if TControl(c) is TShape then
    TShape(c).Pen.Color := Color;
end;

procedure ui_component_set_brushcolor(c: TMyComponent; Color: TMyColor); cdecl;
begin
  if TControl(c) is TShape then
    TShape(c).Brush.Color := Color;
end;

procedure ui_flow_set_scrollable(c: TMyComponent; Value: boolean); cdecl;
begin
  if IsFlow(c) then
  begin
    TTagObject(TControl(c).Tag).FlowProperties.Scrollable := Value;
    flow_apply_properties(c);
  end;
end;

procedure ui_component_set_autowrap(c: TMyComponent; Value: boolean); cdecl;
begin
  if IsFlow(c) then
  begin
    TTagObject(TControl(c).Tag).FlowProperties.AutoWrap := Value;
    flow_apply_properties(c);
  end
  else if TControl(c) is TMemo then
    TMemo(c).WordWrap := Value;
end;

procedure ui_component_set_margin(c: TMyComponent; Value: integer); cdecl;
begin
  if (TControl(c).Parent <> nil) and (TControl(c).Parent.Tag <> 0) then
    Value := Value + TTagObject(TControl(c).Parent.Tag).ChildMargin;
  TControl(c).BorderSpacing.Around := Value;
end;

procedure ui_component_set_childmargin(c: TMyComponent; Value: integer); cdecl;
begin
  if not assigned(TTagObject(TControl(c).Tag)) then
    TControl(c).Tag := PtrInt(TTagObject.Create);
  TTagObject(TControl(c).Tag).ChildMargin := Value;
end;

type
  TOpenControl = class(TWinControl)
    property BorderStyle;
  end;

procedure ui_component_set_border(c: TMyComponent; Value: boolean); cdecl;
begin
  if Value then
    TOpenControl(c).BorderStyle := bsSingle
  else
    TOpenControl(c).BorderStyle := bsNone;
end;

procedure ui_component_set_handler(c: TMyComponent; mask: TEventTypes; Data: TUserCallback); cdecl;
begin
  AllEvents.AddHandler(c, mask, Data);
end;

function ui_create_button(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TButton.Create(TComponent(Form)));
end;

function ui_create_label(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TLabel.Create(TComponent(Form)));
  TLabel(Result).Width := 1000;
  TLabel(Result).AutoSize := True;
  TLabel(Result).ShowAccelChar := False;
end;

function ui_create_edit(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TEdit.Create(TComponent(Form)));
end;

function ui_create_panel(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TPanel.Create(TComponent(Form)));
  TPanel(Result).BorderStyle := bsSingle;
  TPanel(Result).BevelInner := bvNone;
  TPanel(Result).BevelOuter := bvNone;
end;

function ui_create_scroll(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TScrollBox.Create(TComponent(Form)));
  TScrollBox(Result).HorzScrollBar.Tracking := True;
  TScrollBox(Result).VertScrollBar.Tracking := True;
  TScrollBox(Result).BorderStyle := bsSingle;
end;

function ui_create_flow(form: TMyWindow): TMyComponent; cdecl;
var
  flow: TFlowPanel;
begin
  flow := TFlowPanel.Create(TComponent(Form));
  flow.BorderStyle := bsNone;
  flow.BevelInner := bvNone;
  flow.BevelOuter := bvNone;
  flow.FlowStyle := fsLeftRightTopBottom;
  flow.Align := alClient;
  flow.AutoSize := False;
  flow.AutoWrap := False;
  Result := TMyComponent(TScrollBox.Create(TComponent(Form)));
  TScrollBox(Result).BorderStyle := bsNone;
  TScrollBox(Result).HorzScrollBar.Tracking := True;
  TScrollBox(Result).VertScrollBar.Tracking := True;
  TScrollBox(Result).HorzScrollBar.Visible := False;
  TScrollBox(Result).VertScrollBar.Visible := False;
  TControl(Result).Tag := PtrInt(TTagObject.Create);
  flow.Parent := TWinControl(Result);
end;

function ui_create_pages(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TPageControl.Create(TComponent(Form)));
end;

function ui_pages_add_page(c: TMyComponent; Caption: PChar): TMyComponent;
  cdecl;
begin
  Result := TMyComponent(TPageControl(c).AddTabSheet);
  TTabSheet(Result).Caption := Caption;
end;

procedure flow_apply_properties(c: TMyComponent);
var
  flow: TFlowPanel;
  tag: TTagObject;
begin
  flow := TFlowPanel(TWinControl(c).Controls[0]);
  tag := TTagObject(TControl(c).Tag);

  if tag.FlowProperties.Vertical then
    flow.FlowStyle := fsTopBottomLeftRight
  else
    flow.FlowStyle := fsLeftRightTopBottom;

  flow.AutoWrap := tag.FlowProperties.AutoWrap;

  //if tag.FlowProperties.AutoWrap then
  //  flow.Alignment := taLeftJustify
  //else
  //  flow.Alignment := taCenter;

  if tag.FlowProperties.Scrollable then
  begin
    if tag.FlowProperties.Vertical xor tag.FlowProperties.AutoWrap then
    begin
      //scrolls from top to down
      TScrollingWinControl(c).VertScrollBar.Visible := True;
      TScrollingWinControl(c).HorzScrollBar.Visible := False;
      flow.Align := alTop;
    end
    else
    begin
      //scrolls from left to right
      TScrollingWinControl(c).HorzScrollBar.Visible := True;
      TScrollingWinControl(c).VertScrollBar.Visible := False;
      flow.Align := alLeft;
    end;
    if tag.FlowProperties.AutoWrap then
      flow.AutoSize := True
    else
    begin
      flow.AutoSize := False;
      flow_request_manual_scroll(c);
    end;
  end
  else
  begin
    flow.AutoSize := False;
    flow.Align := alClient;
    TScrollingWinControl(c).VertScrollBar.Visible := False;
    TScrollingWinControl(c).HorzScrollBar.Visible := False;
  end;
end;

procedure flow_manual_scroll(c: TMyComponent);
var
  flow: TFlowPanel;
  tag: TTagObject;
  child: TCollectionItem;
  wd: integer;
begin
  flow := TFlowPanel(TWinControl(c).Controls[0]);
  tag := TTagObject(TControl(c).Tag);
  if not (tag.FlowProperties.Scrollable and not tag.FlowProperties.AutoWrap) then
    exit;
  wd := 0;
  if tag.FlowProperties.Vertical xor tag.FlowProperties.AutoWrap then
  begin
    for child in flow.ControlList do
      with TFlowPanelControl(child).Control do
        wd := wd + Height + BorderSpacing.Top + BorderSpacing.Bottom + BorderSpacing.Around;
    flow.ClientHeight := wd;
  end
  else
  begin
    for child in flow.ControlList do
      with TFlowPanelControl(child).Control do
        wd := wd + Width + BorderSpacing.Left + BorderSpacing.Right + BorderSpacing.Around;
    flow.ClientWidth := wd;
  end;
end;

procedure flow_request_manual_scroll(c: TMyComponent);
var
  tag: TTagObject;
begin
  tag := TTagObject(TControl(c).Tag);
  if not (tag.FlowProperties.Scrollable and not tag.FlowProperties.AutoWrap) then
    exit;
  flows_to_process.Add(c);
end;

procedure ui_component_set_font_name(c: TMyComponent; FontName: PChar); cdecl;
begin
  TControl(c).Font.Name := FontName;
end;

procedure ui_component_set_font_size(c: TMyComponent; FontSize: Integer); cdecl;
begin
  TControl(c).Font.Size := FontSize;
end;

procedure ui_component_set_font_color(c: TMyComponent; FontColor: TMyColor);
  cdecl;
begin
  TControl(c).Font.Color := FontColor;
end;

procedure ui_component_set_font_style(c: TMyComponent; Style: Integer); cdecl;
var
  FontStyle: TFontStyles;
begin
  FontStyle := [];
  if Style and 1 > 0 then
    Include(FontStyle, fsBold);
  if Style and 2 > 0 then
    Include(FontStyle, fsItalic);
  if Style and 4 > 0 then
    Include(FontStyle, fsUnderline);
  if Style and 8 > 0 then
    Include(FontStyle, fsStrikeOut);
  TControl(c).Font.Style := FontStyle;
end;

function ui_add_page(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TPageControl.Create(TComponent(Form)));
end;

function ui_create_slider(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TTrackBar.Create(TComponent(Form)));
  TTrackBar(Result).Max := 100;
end;

function ui_create_progressbar(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TProgressBar.Create(TComponent(Form)));
  TProgressBar(Result).Smooth := True;
end;

function ui_create_checkbox(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TCheckBox.Create(TComponent(Form)));
end;

function ui_create_memo(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TMemo.Create(TComponent(Form)));
  TMemo(Result).WordWrap := False;
  TMemo(Result).ScrollBars := ssNone;
end;

function ui_create_listbox(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TListBox.Create(TComponent(Form)));
end;

function ui_create_combobox(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TComboBox.Create(TComponent(Form)));
end;

function ui_create_glpanel(form: TMyWindow): TMyComponent; cdecl;
begin
  SetExceptionMask([exDenormalized, exInvalidOp, exOverflow, exPrecision, exUnderflow, exZeroDivide]);
  Result := TMyComponent(TOpenGLControl.Create(TComponent(Form)));
  TOpenGLControl(Result).AutoResizeViewport := True;
end;

function ui_create_shape(form: TMyWindow; typ: TMyShapeType): TMyComponent; cdecl;
var
  x: TShapeType;
begin
  Result := TMyComponent(TShape.Create(TComponent(Form)));
  case typ of
    Ellipse:
      x := stEllipse;
    Rect:
      x := stRectangle;
    TriaUp:
      x := stTriangle;
    TriaDown:
      x := stTriangleDown;
    TriaLeft:
      x := stTriangleLeft;
    TriaRight:
      x := stTriangleRight;
    Diamond:
      x := stDiamond;
    else
      x := stEllipse;
  end;
  TShape(Result).Shape := x;
end;

function ui_create_flow_wrap(form: TMyWindow): TMyComponent; cdecl;
begin
  Result := TMyComponent(TShape.Create(TComponent(Form)));
  TShape(Result).Shape := stRectangle;
  TShape(Result).Width := 0;
  TShape(Result).Height := 0;
end;




end.
