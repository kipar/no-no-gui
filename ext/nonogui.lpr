library nonogui;

{$mode objfpc}{$H+}

uses
  Classes,
  Interfaces,
  Forms,
  lazopenglcontext,
  uWindow,
  uComponent,
  uTypes,
  uEvents,
  uImages { you can add units after this };

exports

  //basic functions
  ui_create_window,
  ui_component_destroy,
  ui_window_get_visible,
  ui_window_set_fullscreen,
  ui_process,

  //component properties
  ui_component_set_visible,
  ui_component_set_x,
  ui_component_set_y,
  ui_component_set_w,
  ui_component_set_h,
  ui_component_get_x,
  ui_component_get_y,
  ui_component_get_w,
  ui_component_get_h,
  ui_component_set_text,
  ui_component_set_enabled,
  ui_component_set_align,
  ui_component_set_parent,
  ui_component_set_data,
  ui_component_get_text,
  ui_component_set_value,
  ui_component_get_value,
  ui_component_set_checked,
  ui_component_get_checked,
  ui_component_set_index,
  ui_component_get_index,
  ui_component_set_lines_count,
  ui_component_set_line,
  ui_component_set_all_lines,
  ui_component_set_horizscroll,
  ui_component_set_vertscroll,
  ui_component_set_vertical,
  ui_component_set_pencolor,
  ui_component_set_brushcolor,
  ui_component_set_margin,
  ui_component_set_childmargin,
  ui_component_set_autowrap,
  ui_component_set_border,
  ui_component_set_font_name,
  ui_component_set_font_size,
  ui_component_set_font_color,
  ui_component_set_font_style,
  ui_flow_set_scrollable,
  ui_glpanel_get_handle,
  ui_glpanel_swap_buffers,
  ui_glpanel_select,

  //events handling
  ui_component_set_handler,
  ui_events_count,
  ui_event_get,

  //gui controls
  ui_create_button,
  ui_create_label,
  ui_create_edit,
  ui_create_slider,
  ui_create_progressbar,
  ui_create_checkbox,
  ui_create_memo,
  ui_create_listbox,
  ui_create_combobox,
  ui_create_glpanel,
  ui_create_shape,

  //layout controls
  ui_create_panel,
  ui_create_scroll,
  ui_create_flow,
  ui_create_flow_wrap,
  ui_create_pages,
  ui_pages_add_page,

  //images
  ui_load_images,
  ui_create_picture,
  ui_set_image,


  hash_init,
  hash_update,
  hash_size,
  hash_final
;

begin
end.
