# nonogui

No NO GUI! is a gui library for crystal, because others have some problems and NIH.
It uses LCL as a backend and provides DSL to build gui easily.
Basically it is immediate style GUI, but uses native controls and tries to keep performance high.
There are plans to convert it from LCL to GTK4 in future.
It currently works on Windows and Linux (macOS untested)

![Alt text](https://gitlab.com/kipar/no-no-gui/wikis/uploads/73d522a98f9615af38f43fd1c38c1a0e/Screenshot_from_2019-11-27_21-00-54.png)

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     nonogui:
       gitlab: kipar/no-no-gui
   ```
2. Run `shards install`
3. `ext` folder contains sources of pascal library that provides a low level gui functions. So you need to

   3.1. install Lazarus 

   3.2. `lazbuild ./nonogui.lpr` from `ext` folder

   3.3 `sudo cp ./libnonogui.so /usr/lib` (or other location you want).

## Usage

```crystal
require "nonogui"

# Forms inherit from `GUI::Form`
class MainForm < GUI::Form
  # They declare `gui` method that is called to build or update GUI
  def gui
    # add `flow` component as a child. properties can be passed as params.
    flow(scrollable: true) do
      # properties can be set by methods
      align :client
      # now add some children components:
      label "Hello world", x: 20, y: 20
      button "Click me!" do
        on_click do
          puts "Button clicked"
        end
      end
    end
  end
end

# `GUI.run` starts a main GUI loop with a given form as a main form
GUI.run(MainForm.new(width: 800, height: 600))
```

You can check the examples folder for more features (reactive properties, fragments). 

Currently supported components: `button`, `edit`, `label`, `panel`, `scroll`, `flow` (with `wrap`), `slider`, `progressbar`, `checkbox`, `memo`, `listbox`, `combobox`, `glpanel`, `pages` (with `page`), Shapes(`ellipse`, `rect`, `triaup`, `triadown`, `trialeft`, `triaright`, `diamond`), `picture` (requires assets preloading, see examples folder)

Currently supported properties: `x`, `y`, `width`, `height`, `enabled`, `margin`, `childmargin`, `align`, `text`, `border`, `font_name`, `font_size`, `font_style`, `font_color`, depending on component: `vertical`, `scrollable`, `horiz_scroll`, `vert_scroll`, `autowrap`, `value`, `index`, `pen`, `brush`, `checked`, `image`

Currently supported events: `on_mouse_move`, `on_mouse_border`, `on_keyboard`, `on_click`, depending on component: `on_change`, `on_select`

Documentation and proper instructions are planned.

## Development

- [ ] GTK4 (there is a branch for it, but it's not working yet)
- [ ] More components
  - [ ] treeview
  - [ ] grid
  - [ ] toggle?
  - [ ] radio?
  - [ ] spin
  - [ ] popup
  - [ ] std dialogs
- [ ] Test on macOS
- [ ] Test on qT
- [ ] CI?

## Contributors

- [konovod](https://gitlab.com/kipar) - creator and maintainer
