require "./libnonogui"
require "./base"
require "./elements"

module GUI
  class Component
    macro indirect_gui_property(internal, item, *, active = false)
      @[IndirectGuiProperty]
      @{{item}}
      {% if active %} @{{internal}}_dependents_actual = false {% end %}

      def {{item.var.id}}=(value : {{item.type}})
        @{{item.var.id}} = value
        recalculate_{{internal}} # TODO - recalculate once per update
      end

      {% if active %}
      def {{item.var.id}} : {{item.type}}
        unless @{{internal}}_dependents_actual
          @{{internal}}_dependents_actual = true
          updated_{{internal}}(self.{{internal}})
        end
        @{{item.var.id}}
      end
      {% else %}
      def {{item.var.id}} : {{item.type}}
        @{{item.var.id}}
      end
      {% end %}

      def {{item.var.id}} (value : {{item.type}})
        self.{{item.var.id}} = value
      end

    end
  end

  class ImageButton
    gui_property image : Image = Image.new(0)
    indirect_gui_property image, image_normal : Image = Image.new(0)
    indirect_gui_property image, image_hover : Image?
    indirect_gui_property image, image_pressed : Image?
    indirect_gui_property image, image_disabled : Image?

    private def apply_args(**args)
      super(**args)
      recalculate_image
    end

    @mouse_inside = false
    @pressed = false

    def enabled=(value)
      super
      recalculate_image
    end

    def recalculate_image
      self.image = if !@enabled
                     @image_disabled || @image_normal
                   elsif @pressed
                     @image_pressed || @image_normal
                   elsif @mouse_inside
                     @image_hover || @image_normal
                   else
                     @image_normal
                   end
    end

    def own_on_mouse_border(evt)
      @mouse_inside = evt.entered
      @pressed = false unless @mouse_inside
      recalculate_image
    end

    def own_on_mouse_button(evt)
      @pressed = evt.down
      recalculate_image
    end
  end
end
