require "./libnonogui"

module GUI
  private enum ShowingMode
    Initialization
    Build
    Rebuild
    # DebugBuild
    # DebugRebuild
    Event
  end

  private record GlobalState, mode : ShowingMode, cur_window : Form?, cur_component : Array(Component), cur_index : Array(Int32), active_forms : Array(Form), forms_to_init : Array(Form), redraw_all : Bool do
    setter mode
    setter redraw_all
    # TODO - allow to create forms in `Form#gui`
    setter cur_window
  end

  STATE = GlobalState.new(ShowingMode::Initialization, nil, Array(Component).new, Array(Int32).new, Array(Form).new, Array(Form).new, false)

  annotation GuiProperty
  end
  annotation GuiActiveProperty
  end
  annotation GuiEvent
  end
  annotation IndirectGuiProperty
  end

  abstract class Component
    macro gui_property(item, *, active = false, internal = false)
      @[GuiProperty(name: {{internal || item.var}})]
      {% if active %} @[GuiActiveProperty] {% end %}
      @{{item}}
      @{{item.var.id}}_changed = false
      {% if active %} @{{item.var.id}}_actual = false {% end %}

      def {{item.var.id}}=(value : {{item.type}})
        @changed = true
        @{{item.var.id}}_changed |= @{{item.var.id}} != value
        # p "applying {{item.var.id}}=#{value} to #{self.class.name}"if @{{item.var.id}}_changed
        @{{item.var.id}} = value
      end

      {% if active %}
      def {{item.var.id}} : {{item.type}}
        unless @{{item.var.id}}_actual
          @{{item.var.id}}_actual = true
          @{{item.var.id}} = {{item.type}}.new(NONOGUI.get_{{internal || item.var}}(@handle))
        end
        @{{item.var.id}}
      end
      {% else %}
      def {{item.var.id}} : {{item.type}}
        @{{item.var.id}}
      end
      {% end %}


      private def fetch_{{item.var.id}} : {{item.type}}
        @{{item.var.id}}_changed = false
        @{{item.var.id}}
      end

      def {{item.var.id}} (value : {{item.type}})
        self.{{item.var.id}} = value
      end
    end

    # TODO macro gui_miltiline_property

    macro gui_event(typ, item)
      @[GuiEvent(typ: {{typ}})]
      property {{item.id}} : Proc(NONOGUI::{{typ.id}}Event, Nil)?

      def {{item.id}}(&block : Proc(NONOGUI::{{typ}}Event, Nil))
        if self.{{item.id}} != block
          self.{{item.id}} = block
          unless self.responds_to? :own_{{item.id}}
            NONOGUI.set_handler self.handle, NONOGUI::EventTypes::{{typ}}, block.unsafe_as(NONOGUI::UserCallback)
          end
        end
      end
    end

    gui_property x : Int32 = 0, active: true
    gui_property y : Int32 = 0, active: true
    gui_property width : Int32 = 0, internal: w, active: true
    gui_property height : Int32 = 0, internal: h, active: true
    gui_property enabled : Bool = true
    gui_property visible : Bool = false, active: true
    gui_property margin : Int32 = 0
    gui_property childmargin : Int32 = 0
    gui_property align : NONOGUI::Align = NONOGUI::Align::None

    gui_event MouseMove, on_mouse_move
    gui_event MouseBorder, on_mouse_border
    gui_event MouseButton, on_mouse_button
    gui_event Keyboard, on_keyboard
    gui_event Click, on_click

    property changed : Bool = true
    property handle : NONOGUI::MyComponent = NONOGUI::MyComponent.new(0)

    getter children = [] of Component
    getter! parent : Component

    private def self.same(existing, **args)
      existing.class == self
    end

    protected abstract def create_handle(parent) : NONOGUI::MyComponent

    private def reset_active_properties
      {% for ivar in @type.instance_vars.select &.annotation(GuiActiveProperty) %}
         @{{ivar}}_actual = false
      {% end %}
    end

    def update_properties
      if @changed
        @changed = false

        {% for ivar in @type.instance_vars.select(&.annotation(GuiProperty)) %}
        {% int_name = ivar.annotation(GuiProperty)[:name] %}
          NONOGUI.set_{{int_name.id}}(@handle, fetch_{{ivar}}) if @{{ivar}}_changed
        {% end %}
      end
      reset_active_properties
    end

    private def apply_args(**args)
      detected = 0
      {% for ivar in @type.instance_vars.select(&.annotation(IndirectGuiProperty)) %}
        if args[:{{ivar}}]?
          detected += 1
          self.{{ivar}} = args[:{{ivar}}]?.not_nil!
        end
      {% end %}
      {% for ivar in @type.instance_vars.select(&.annotation(GuiProperty)) %}
        if args[:{{ivar}}]?
          detected += 1
          self.{{ivar}} = args[:{{ivar}}]?.not_nil!
          @{{ivar}}_changed = true
        end
      {% end %}
      # TODO - detect exact names
      raise "Some arguments (#{args.size - detected}) don't match gui properties: #{args}" if detected < args.size
    end

    private def apply_own_handlers
      {% for ivar in @type.instance_vars.select &.annotation(GuiEvent) %}
        if self.responds_to? :own_{{ivar}}
          {% typ = ivar.annotation(GuiEvent)[:typ] %}
          block = Proc(NONOGUI::{{typ.id}}Event, Nil).new do |evt|
            self.own_{{ivar}}(evt)
            if %x = self.{{ivar}}
              %x.call(evt)
            end
            nil
          end
          NONOGUI.set_handler self.handle, NONOGUI::EventTypes::{{typ}}, block.unsafe_as(NONOGUI::UserCallback)
        end
      {% end %}
    end

    def initialize(@parent, **args)
      apply_args(**args)
      @handle = create_handle(@parent)
      apply_own_handlers()
      if par = @parent
        NONOGUI.set_parent handle, par.handle
      end
    end

    def self.show(**args)
      show(**args) { }
    end

    macro default_gui_property(item)
      def self.show(def_value, **args)
        show(**args, {{item}}: def_value)
      end
      def self.show(def_value, **args, &)
        show(**args, {{item}}: def_value) { |x| yield(x) }
      end
    end

    private def self.solve_conflict(parent, index, **args)
      # TODO - more heuristics?
      p "solving conflict need #{self} exists #{parent.children[index].class}"
      1000.times do |i|
        if parent.children.size > index + i && self.same(parent.children[index + i], **args)
          p "dropping #{i} elements"
          i.times { parent.children[index].destroy }
          return parent.children[index]
        end
      end
      # TODO - insert child
      p "dropping all #{parent.children.size - index} elements"
      parent.truncate_children(index)
      add_element parent, **args
    end

    def destroy
      NONOGUI.destroy @handle
      parent.children.delete self
    end

    protected def truncate_children(new_count)
      (@children.size - new_count).times { @children[-1].destroy }
    end

    private def self.add_element(parent, **args)
      self.new(parent, **args).tap do |item|
        parent.children << item
      end
    end

    def self.show(**args, &block)
      parent = STATE.cur_component[-1]
      existing_id = STATE.cur_index[-1]
      if parent && existing_id == 0
        parent.update_properties
      end
      case STATE.mode
      when .build?
        item = add_element(parent, **args)
      else
        if parent.children.size <= existing_id
          item = add_element(parent, **args)
        else
          old = parent.children[existing_id]
          if self.same(old, **args)
            item = old
          else
            item = solve_conflict(parent, existing_id, **args)
          end
        end
      end
      # recursive call
      STATE.cur_component.push(item)
      STATE.cur_index.push(0)
      yield(item)
      item.truncate_children(STATE.cur_index[-1])
      item.update_properties
      STATE.cur_index.pop
      STATE.cur_component.pop
      STATE.cur_index[-1] = STATE.cur_index[-1] + 1
      item
    end
  end

  abstract class Customizable < Component
  end

  class Form < Customizable
    property should_hide = false
    getter was_shown = false
    gui_property text : String = "", active: false
    gui_property fullscreen : Bool = false

    def initialize(**args)
      apply_args(**args)
      @handle = NONOGUI::MyComponent.new(0)
      STATE.forms_to_init << self
    end

    def gui
    end

    def show
      self.visible = true
      @should_hide = false
      if @handle != NONOGUI::MyComponent.new(0)
        self.update_properties
        STATE.active_forms << self unless STATE.active_forms.includes?(self)
      end
    end

    protected def actual_init
      @handle = create_handle(@parent)
      call_gui(ShowingMode::Build)
    end

    def hide
      self.visible = false
      @should_hide = true
    end

    protected def call_gui(mode)
      STATE.mode = mode
      STATE.cur_window = self
      STATE.cur_component.push(self)
      STATE.cur_index.push(0)
      gui
      update_properties
      STATE.cur_index.pop
      STATE.cur_component.pop
      STATE.cur_window = nil
    end

    def create_handle(parent) : NONOGUI::MyComponent
      NONOGUI.create_window
    end
  end

  def self.redraw # TODO - partial redraw
    STATE.redraw_all = true
  end

  def self.run(mainform)
    mainform.actual_init
    STATE.forms_to_init.each { |f| f.actual_init unless f == mainform }
    STATE.forms_to_init.clear
    mainform.show
    loop do
      # Fiber.yield
      sleep(0.001)
      NONOGUI.process
      changed = STATE.redraw_all
      STATE.redraw_all = false
      NONOGUI.events_count.times do |i|
        evt = NONOGUI.event_get(i).value
        typ = NONOGUI::EventType.new(evt.typ)
        # p typ, evt
        case typ
        when .quitting?
          exit
        else
          evt.callback.unsafe_as(Proc(NONOGUI::Event, Nil)).call(evt)
          changed = true
        end
      end
      STATE.active_forms.each &.call_gui(ShowingMode::Rebuild) if changed
      STATE.active_forms.reject! &.should_hide
      STATE.forms_to_init.each { |f| f.actual_init }
      STATE.forms_to_init.clear
    end
  end
end
