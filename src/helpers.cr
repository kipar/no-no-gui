require "./libnonogui"

module GUI
  # :nodoc:
  def Bool.new(x : Bool)
    x
  end

  class Component
    def al(x : NONOGUI::Align)
      x
    end
  end
end
