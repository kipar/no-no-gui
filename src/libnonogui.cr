@[Link("nonogui")]
lib NONOGUI
  alias MyComponent = Pointer(Void)
  alias MyWindow = Pointer(Void)
  alias Data = Pointer(Void)
  alias Hash = Pointer(Void)

  enum Align
    None
    Client
    Top
    Bottom
    Left
    Right
    Center
  end

  enum EventType
    Quitting
    Click
    Change
    MouseButton
    MouseMove
    MouseBorder
    Keyboard
  end

  @[Flags]
  enum EventTypes
    Quitting
    Click
    Change
    MouseButton
    MouseMove
    MouseBorder
    Keyboard
  end

  # alias UserCallback = Proc(Nil, Nil)

  # record( UserCallback, ptr1 : Pointer(Void), ptr2 : Pointer(Void))
  struct UserCallback
    ptr1 : Pointer(Void)
    ptr2 : Pointer(Void)
  end

  struct Event
    callback : UserCallback
    typ : Int32
    data : Int32[4]
  end

  struct ClickEvent
    callback : UserCallback
    typ : Int32
  end

  struct MouseButtonEvent
    callback : UserCallback
    typ : Int32
    x : Int32
    y : Int32
    button : Int32
    down : Bool
  end

  struct MouseMoveEvent
    callback : UserCallback
    typ : Int32
    x : Int32
    y : Int32
    shift : Int32
  end

  struct ChangeEvent
    callback : UserCallback
    typ : Int32
    value : Int32
    text : UInt8*
  end

  struct MouseBorderEvent
    callback : UserCallback
    typ : Int32
    entered : Bool
  end

  struct KeyboardEvent
    callback : UserCallback
    typ : Int32
    scancode : Int32
    # sym : Char
    down : Bool
  end

  enum ShapeType
    Ellipse
    Rect
    TriaUp
    TriaDown
    TriaLeft
    TriaRight
    Diamond
  end

  @[Flags]
  enum FontStyle : Int32
    Bold      = 1
    Italic    = 2
    Underline = 4
    Strikeout = 8
  end

  alias Color = UInt32

  alias ImageHandle = UInt32

  fun create_window = ui_create_window : MyWindow
  fun process = ui_process
  fun destroy = ui_component_destroy(c : MyComponent)
  fun get_visible = ui_window_get_visible(c : MyWindow) : Bool
  fun set_fullscreen = ui_window_set_fullscreen(c : MyComponent, value : Bool)

  fun set_visible = ui_component_set_visible(c : MyComponent, value : Bool)
  fun set_x = ui_component_set_x(c : MyComponent, value : Int32)
  fun set_y = ui_component_set_y(c : MyComponent, value : Int32)
  fun set_w = ui_component_set_w(c : MyComponent, value : Int32)
  fun set_h = ui_component_set_h(c : MyComponent, value : Int32)

  fun get_x = ui_component_get_x(c : MyComponent) : Int32
  fun get_y = ui_component_get_y(c : MyComponent) : Int32
  fun get_w = ui_component_get_w(c : MyComponent) : Int32
  fun get_h = ui_component_get_h(c : MyComponent) : Int32

  fun set_text = ui_component_set_text(c : MyComponent, value : UInt8*)
  fun set_enabled = ui_component_set_enabled(c : MyComponent, value : Bool)
  fun set_align = ui_component_set_align(c : MyComponent, value : Align)
  fun set_parent = ui_component_set_parent(c : MyComponent, value : MyComponent)
  fun set_data = ui_component_set_data(c : MyComponent, value : Data)
  fun set_handler = ui_component_set_handler(c : MyComponent, mask : EventTypes, value : UserCallback)

  fun set_value = ui_component_set_value(c : MyComponent, value : Int32)
  fun set_checked = ui_component_set_checked(c : MyComponent, value : Bool)
  fun set_index = ui_component_set_index(c : MyComponent, value : Int32)
  fun set_lines_count = ui_component_set_lines_count(c : MyComponent, value : Int32)
  fun set_line = ui_component_set_line(c : MyComponent, index : Int32, value : UInt8*)
  fun set_all_lines = ui_component_set_all_lines(c : MyComponent, value : UInt8*)
  fun set_vertical = ui_component_set_vertical(c : MyComponent, value : Bool)
  fun set_pencolor = ui_component_set_pencolor(c : MyComponent, color : Color)
  fun set_brushcolor = ui_component_set_brushcolor(c : MyComponent, color : Color)
  fun set_margin = ui_component_set_margin(c : MyComponent, value : Int32)
  fun set_childmargin = ui_component_set_childmargin(c : MyComponent, value : Int32)
  fun set_flow_scrollable = ui_flow_set_scrollable(c : MyComponent, value : Bool)
  fun set_autowrap = ui_component_set_autowrap(c : MyComponent, value : Bool)
  fun set_horiz_scroll = ui_component_set_horizscroll(c : MyComponent, value : Bool)
  fun set_vert_scroll = ui_component_set_vertscroll(c : MyComponent, value : Bool)
  fun set_border = ui_component_set_border(c : MyComponent, value : Bool)
  fun set_font_name = ui_component_set_font_name(c : MyComponent, name : UInt8*)
  fun set_font_size = ui_component_set_font_size(c : MyComponent, size : Int32)
  fun set_font_color = ui_component_set_font_color(c : MyComponent, color : Color)
  fun set_font_style = ui_component_set_font_style(c : MyComponent, style : FontStyle)

  fun get_text = ui_component_get_text(c : MyComponent) : UInt8*
  fun get_value = ui_component_get_value(c : MyComponent) : Int32
  fun get_index = ui_component_get_index(c : MyComponent) : Int32
  fun get_checked = ui_component_get_checked(c : MyComponent) : Bool

  fun glpanel_get_handle = ui_glpanel_get_handle(c : MyComponent) : Void*
  fun glpanel_swap_buffers = ui_glpanel_swap_buffers(c : MyComponent)
  fun glpanel_select = ui_glpanel_select(c : MyComponent)

  fun events_count = ui_events_count : Int32
  fun event_get = ui_event_get(index : Int32) : Pointer(Event)

  fun create_button = ui_create_button(w : MyWindow) : MyComponent
  fun create_label = ui_create_label(w : MyWindow) : MyComponent
  fun create_edit = ui_create_edit(w : MyWindow) : MyComponent
  fun create_slider = ui_create_slider(w : MyWindow) : MyComponent
  fun create_progressbar = ui_create_progressbar(w : MyWindow) : MyComponent
  fun create_checkbox = ui_create_checkbox(w : MyWindow) : MyComponent
  fun create_memo = ui_create_memo(w : MyWindow) : MyComponent
  fun create_listbox = ui_create_listbox(w : MyWindow) : MyComponent
  fun create_combobox = ui_create_combobox(w : MyWindow) : MyComponent
  fun create_glpanel = ui_create_glpanel(w : MyWindow) : MyComponent
  fun create_shape = ui_create_shape(w : MyWindow, typ : ShapeType) : MyComponent
  fun create_panel = ui_create_panel(w : MyWindow) : MyComponent
  fun create_scroll = ui_create_scroll(w : MyWindow) : MyComponent
  fun create_flow = ui_create_flow(w : MyWindow) : MyComponent
  fun create_flow_wrap = ui_create_flow_wrap(w : MyWindow) : MyComponent
  fun create_pages = ui_create_pages(w : MyWindow) : MyComponent
  fun pages_add_page = ui_pages_add_page(c : MyComponent, caption : UInt8*) : MyComponent

  fun load_images = ui_load_images(images_dir : UInt8*, md5hash : UInt8*) : Bool
  fun create_picture = ui_create_picture(w : MyWindow) : MyComponent
  fun set_image = ui_set_image(c : MyComponent, img : ImageHandle)

  fun hash_init : Hash
  fun hash_update(hash : Hash, data : UInt8*, size : Int32)
  fun hash_size(hash : Hash) : Int32
  fun hash_final(hash : Hash, result : UInt8*)
end
