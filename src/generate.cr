require "./libnonogui"

private def fine_name(s)
  return s if s == "RES"
  s = s.gsub(/\..{0,3}$/, "") # drop extension
  s = s.chars.select { |c| c.to_s =~ /[[:alnum:]_]/ }.join
  s = "id_" + s if s == "" || s[0] =~ /[[:digit:]]/
  s.capitalize
end

# GRAPH_EXTS = ["bmp", "jpg", "png"]

private class Generator
  @res_ids = Hash(String, Int32).new(0)
  @indent = 0
  @checksum : NONOGUI::Hash
  @basedir = ""

  def print_res(res, item, fullpath)
    NONOGUI.hash_update(@checksum, fullpath, fullpath.bytesize)
    item = fine_name(item)
    print "#{item} = #{res}.new(#{@res_ids[res]})"
    @res_ids[res] = @res_ids[res] + 1
  end

  def print(s)
    @io.puts "#{" "*@indent}#{s}"
  end

  def process_dir(short, adir)
    print "module #{fine_name(short)}"
    @indent += 2
    list = [] of String
    Dir.each(@basedir + adir) do |item|
      next if item == "." || item == ".."
      list << item if Dir.exists?(@basedir + adir + item)
    end
    list.sort
    list.each { |item| process_dir item, adir + item + '/' }

    list.clear
    Dir.each(@basedir + adir) do |item|
      next if item == "." || item == ".."
      unless Dir.exists?(@basedir + adir + item)
        list << item
      end
    end
    list.sort
    list.each { |item| print_res("GUI::Image", item, adir + item) }

    @indent -= 2
    print "end"
  end

  def initialize(path, @io : IO)
    @checksum = NONOGUI.hash_init
    @basedir = path + '/'
    process_dir "RES", ""
    print ""
    check_result = Bytes.new(NONOGUI.hash_size(@checksum))
    NONOGUI.hash_final(@checksum, check_result)
    print "module RES"
    print "  IMAGES_DIR = \"#{path}\""
    print "  IMAGES_CHECKSUM = \"#{String.new(check_result)}\""
    print "end"
  end
end

Generator.new(ARGV[0], STDOUT)
