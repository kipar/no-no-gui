require "./base"

module GUI
  class Fragment < Customizable
    # :nodoc:
    def create_handle(parent) : NONOGUI::MyComponent
      NONOGUI::MyComponent.new(0)
    end

    def initialize
    end
  end
end
