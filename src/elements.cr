require "./libnonogui"
require "./base"

module GUI
  {% for typ in %i(Button Edit Label Panel CheckBox Memo ListBox ComboBox) %}
    class {{typ.id}} < Component
      gui_property font_name : String = "default"
      gui_property font_size : Int32 = 0
      gui_property font_color : UInt32 = 0
      gui_property font_style : NONOGUI::FontStyle = NONOGUI::FontStyle::None
      gui_property text : String = "", active: {{typ == :Edit || typ == :Memo || typ == :ComboBox}}
      # :nodoc:
      def create_handle(parent) : NONOGUI::MyComponent
        NONOGUI.create_{{typ.downcase.id}} STATE.cur_window.not_nil!.handle
      end
    end
  {% end %}

  {% for typ in %i(Scroll Flow Slider ProgressBar GLPanel Pages Picture) %}
    class {{typ.id}} < Component
      # :nodoc:
      def create_handle(parent) : NONOGUI::MyComponent
        NONOGUI.create_{{typ.downcase.id}} STATE.cur_window.not_nil!.handle
      end
    end
  {% end %}

  {% for typ in %i(ImageButton ImageCheckBox) %}
    class {{typ.id}} < Component
      # :nodoc:
      def create_handle(parent) : NONOGUI::MyComponent
        NONOGUI.create_picture STATE.cur_window.not_nil!.handle
      end
    end
  {% end %}

  {% for typ in %i(Ellipse Rect TriaUp TriaDown TriaLeft TriaRight Diamond) %}
    class {{typ.id}} < Component
      gui_property pen : UInt32 = 0, internal: pencolor
      gui_property brush : UInt32 = 0xFFFFFFFF, internal: brushcolor
      # :nodoc:
      def create_handle(parent) : NONOGUI::MyComponent
        NONOGUI.create_shape STATE.cur_window.not_nil!.handle, NONOGUI::ShapeType::{{typ.id}}
      end
    end
  {% end %}

  class FlowWrap < Component
    # :nodoc:
    def create_handle(parent) : NONOGUI::MyComponent
      NONOGUI.create_flow_wrap STATE.cur_window.not_nil!.handle
    end
  end

  {% for typ in %i(Button Edit Label Panel Scroll Flow Slider ProgressBar CheckBox Memo ListBox ComboBox GLPanel Pages Ellipse Rect TriaUp TriaDown TriaLeft TriaRight Diamond Picture ImageButton ImageCheckBox) %}
    class Customizable
      def {{typ.downcase.id}}(def_value, **args, &)
        {{typ.id}}.show(def_value, **args) do |x|
          with x.as({{typ.id}}) yield(x.as({{typ.id}}))
        end.as({{typ.id}})
      end

      def {{typ.downcase.id}}(def_value, **args)
        {{typ.id}}.show(def_value, **args).as({{typ.id}})
      end

      def {{typ.downcase.id}}(**args, &)
        {{typ.id}}.show(**args) do |x|
          with x.as({{typ.id}}) yield(x.as({{typ.id}}))
        end.as({{typ.id}})
      end

      def {{typ.downcase.id}}(**args)
        {{typ.id}}.show(**args).as({{typ.id}})
      end

    end
  {% end %}

  class Flow
    gui_property vertical : Bool = false
    gui_property scrollable : Bool = false, internal: flow_scrollable
    gui_property autowrap : Bool = false
    gui_property border : Bool = false

    def wrap
      FlowWrap.show
    end
  end

  class Label
    default_gui_property text
  end

  class Button
    default_gui_property text
  end

  class Panel
    gui_property border : Bool = true
  end

  class Scroll
    gui_property vert_scroll : Bool = true
    gui_property horiz_scroll : Bool = true
    gui_property border : Bool = true
  end

  class ProgressBar
    gui_property vertical : Bool = false
    gui_property value : Int32 = 50
    default_gui_property value
  end

  class Edit
    gui_event Change, on_change
    default_gui_property text
  end

  class Memo
    gui_property vert_scroll : Bool = false
    gui_property horiz_scroll : Bool = false
    gui_property autowrap : Bool = false
    gui_property border : Bool = true
    gui_event Change, on_change
  end

  class Slider
    gui_property vertical : Bool = false
    gui_property value : Int32 = 50, active: true
    default_gui_property value
    gui_event Change, on_change
  end

  class ListBox
    gui_property index : Int32 = -1, active: true
    gui_property border : Bool = true
    gui_event Change, on_change
  end

  class ComboBox
    gui_property index : Int32 = -1, active: true
    gui_property items : String = "", internal: all_lines
    gui_event Change, on_change
  end

  class CheckBox
    gui_property checked : Bool = false, active: true
    default_gui_property text
    gui_event Change, on_change
  end

  class Page < Component
    gui_event Change, on_select
    gui_property text : String = ""
    default_gui_property text

    def create_handle(parent) : NONOGUI::MyComponent
      NONOGUI.pages_add_page parent.not_nil!.handle, @text
    end
  end

  class Pages
    gui_event Change, on_change
    gui_property index : Int32 = 0, active: true

    def page(**args, &)
      Page.show(**args) do |x|
        with x.as(Page) yield(x.as(Page))
      end.as(Page)
    end

    def page(**args)
      Page.show(**args).as(Page)
    end

    def page(def_value, **args, &)
      Page.show(def_value, **args) do |x|
        with x.as(Page) yield(x.as(Page))
      end.as(Page)
    end

    def page(def_value, **args)
      Page.show(def_value, **args).as(Page)
    end
  end

  record Image, handle : UInt32 do
    def to_u32!
      @handle
    end
  end

  class Picture
    gui_property image : Image = Image.new(0)
    default_gui_property image
  end

  macro load_images(dir)
    {{ run "./generate", dir }}
    unless NONOGUI.load_images RES::IMAGES_DIR, RES::IMAGES_CHECKSUM
      raise "Directory #{RES::IMAGES_DIR} not exists or checksum do not match, run generate.cr on it"
    end
  end

  class GLPanel
    @@current : GLPanel?

    def gl_handle
      NONOGUI.glpanel_get_handle @handle
    end

    def render(&block)
      unless @@current == self
        NONOGUI.glpanel_select @handle
        @@current = self
      end
      yield
      NONOGUI.glpanel_swap_buffers @handle
    end
  end
end
